---
lang: de
permalink: /
layout: default
---

{% include map.md %}

# Karte des Kongresses
{:.page-heading }

Klicke auf dem entsprechenden Bereich um mehr darüber zu erfahren.

Alternativ gibt es auch eine [Listenübersicht](spaces_list).
