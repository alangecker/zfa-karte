---
layout: stream
title:  Futur III
stream_date:
  begin: 2020-08-25 10:00:00 +02:00 # Time
  end:   2020-08-29 21:30:00 +02:00 # Time
svg: analog
---

## Willkommen to Futur III

We will stream Wir bedanken uns herzlich bei 
ecapio, dass sie uns für die Streams im Futur III ihren Youtube-Kanal zur Verfügung stellen.

### Nächste Veranstaltung
**29.08., 15:00-16:30: Podium - International finance and global justice with Anne Löscher, Daniela Gabor, Ndongo Sylla**

<iframe class="board16-9" src="https://www.youtube.com/embed/Rb51npq6JBA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


If you need translation, click on the language button below and open it in a new tab.

[Deutsches Audio](http://stream.foodada.de:8000/DE3){:.btn.centered}


## Programme

### Saturday, 29.08.

***Time:*** 15:00-16:30

***Event:*** Panel - International Finance and Global Justice – a brief look at finance, colonial continuities and the climate crisis

***Speakers:*** Anne Löscher, Daniela Gabor, Ndongo Sylla

***Translation:*** DE

***Description:*** The question of how to achieve climate justice, to end persistent neo-colonial relations and inequality cannot be answered without looking into the realm of international finance. The panel discussion will hence touch upon each of these aspects. N’dongo Sylla speaks on a progressive Pan-Africanist alternative to French monetary imperialism. Daniela Gabor provides some insights into the seemingly opaque real of current financial practices. Anne Löscher sheds light on the interaction between finance and the climate crisis at the periphery.
