---
layout: stream
title:  Futur I
stream_date:
  begin: 2020-08-25 10:00:00 +02:00 # Time
  end:   2020-08-29 21:30:00 +02:00 # Time
svg: analog
---
## Welcome to Futur I
A large portion of the livestreams will be broadcasted right here. Enjoy!

### Next Stream
**25.08., 16:00-18:05 Kick-off congress with Bini Adamczak, Theater X, Kai Kuhnhenn and Nina Treu"**

<iframe class="board16-9" src="https://www.youtube.com/embed/ZjSbOpK6ws4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>



**If you need translation, right click on the language button below and open it in a new tab.**


[Deutsches Audio](http://stream.foodada.de:8000/DE1){:.btn.centered}
[English audio](http://stream.foodada.de:8000/EN1){:.btn.centered}
[Audio Español](http://stream.foodada.de:8000/ES1){:.btn.centered}
[Audio Français](http://stream.foodada.de:8000/FR1){:.btn.centered}

## Programm

Programme on the ZFA channel Futur 1

### Tuesday, August 25th 

---

***Time:*** 10:00 a.m. 

***Event*** Press Conference Future For All
Audio channel for English translation: http://stream.foodada.de:8000/EN1

---

***Time:*** 04:00-06:05 p.m.

***Event*** Kick-off congress.

***Speakers:*** Bini Adamczak, Theater X, Kai Kuhnhenn and Nina Treu.

Audio channel for English translation: http://stream.foodada.de:8000/EN1 

***Description:*** The political author and artist Bini Adamczak approaches the subject of utopias in a philosophical way and asks the question: Utopia, but how? Afterwards, Kai Kuhnhenn and Nina Treu from Konzeptwerk will present the most important results of the "Future for All" project initiated in 2019. 

---

***Time:*** 08:00-09:45 p.m.

***Event*** Opening Panel - Building the Future from the Crises of Today

***Speakers:*** Antje von Broock, Tonny Nowshin, Tina Röthig and Ruth Krohn

Audio channel for English translation: http://stream.foodada.de:8000/EN1

***Description:*** In the midst of crises and everyday struggles, the question of utopia arises. What would an ecological economy and just way of life look like? Using the examples of the climate crisis and corona pandemic, speakers* discuss their utopias and solutions. How much redistribution do we need? Can a strong Green New Deal avert the worst damages of climate change? Is it enough to regulate the market economy or does it fundamentally prevent a climate-friendly world? How far-reaching must social changes be?

### Wednesday, August 26th 

---

***Time:*** 09:30-10:30 a.m.

***Event*** Morning Start with Natasha A. Kelly.

Audio channel for English translation: http://stream.foodada.de:8000/EN1

***Description:*** We start the congress day together. You can expect a mixture of organisational matters, a substantive impulse and exchange. The heart of the event is the keynote, which will provide us with inspiration for the day. Invited ***Speakers:*** Dr. Natasha A. Kelly (Afrofuturism), Julia Fritzsche (journalist) and Derya Binışık (Reproductive Rights and Social Justice) and two people from the congress organisation. They present their vision from different positions on utopia and transformation - intersectional feminist.

---

***Time:*** 11:00 a.m.-12:30 p.m.

***Event*** Housing 2048 - No free market, no state, no contracts?

***Speakers:*** Christoph Struenke, Stefania Koller, Mary Lürtzing, Rouzbeh Taheri and Lina Hurlin.

Audio channel for English translation: http://stream.foodada.de:8000/EN1

***Description:*** What does housing look like in our visions in 2048 and how did we get there? After an exchange about the speakers’ utopias, we want to delve deeper into several questions. How are apartments and houses financed in 2048 and how are they organised? How is living space distributed and who decides that? Who provides housing in the first place? How is decided what is built and where and how? And who takes care of the maintenance of living space and houses?

---

***Time:*** 03:00-04:30 p.m.

***Event*** Conditions of food production and distribution in a just food system.

***Speakers:*** Nora McKeon, Julianna Fehlinger, Aboubakar Soumahoro and Marie Populus.
Audio channel for German translation: http://stream.foodada.de:8000/DE1
Audio channel for English translation: http://stream.foodada.de:8000/EN1
Audio channel for Italian translation: http://stream.foodada.de:8000/ITA1

***Description:*** In this panel we discuss three interdependent sectors of an utopian, just food system: production, distribution and governance - focusing on the social and societal dimension. How do good working conditions look like and how do we get there? How is a democratic and participative distribution organized? What kind of policy do we need in order to enable food sovereignty? We take a look at an utopian system and discuss its democratic implications with three experts and practitioners.

---

***Time:*** 05:00-06:30 p.m.

***Event*** Power for the Future for all: the Question of Energy.

***Speakers:*** Tonny Nowshin, Linda Schneider, Nguy Thi Khanh and Lebogang Mulaisi.

Audio channel for German translation: http://stream.foodada.de:8000/DE1

Audio channel for English translation: http://stream.foodada.de:8000/EN1

***Description:*** Our use of energy is closely linked to how we live and produce. At the same time, the energy system is a major driver of the climate crisis, as well as a source of exploitation and violent conflicts across the world, and many people continue to be excluded from energy access. Only a few years remain for the urgently needed transformation. We want to discuss how the energy system of the future could both address climate change and meet people's needs – and how we can shape the path towards this goal.

---

***Time:*** 08:00-09:45 p.m.

***Event*** Economy of the future - How do we organize democratic and need-oriented (re)production?

***Speakers:*** Nina Treu, Nic Odenwälder, Simon Sutterluetti, Natalia Lizama L. and Andrea Vetter.
Audio channel for English translation: http://stream.foodada.de:8000/EN1

***Description:*** The global crises show: we must agree on the outlines of an alternative to capitalism. How could we organise production, reproduction and distribution democratically and based on needs? Is there still a market in the economy in 2048, or more state planning? What could global coordination without domination look like? Do we still have to do wage labor for our existence or is a utopia based primarily on motivation and self-selection? In this panel, we try to develop some answers together.

### Thursday, August 27th 

---

***Time:*** 09:30-10:30 a.m.

***Event*** Morning Start with Kate Čabanová and Werner Rätz.

Audio channel for English translation: http://stream.foodada.de:8000/EN1

---

***Time:*** 11:00-12:30 a.m.

***Event*** We need an Economic Transformation! NOW

***Speakers:*** Silke Helfrich, Christian Felber, Matthias Schmelzer and Karin Walter.

Audio channel for English translation: http://stream.foodada.de:8000/EN1

***Description:*** Do alternative economies need a common strategy to build a solidarity-based economy? Yes, do representatives of various alternative-economic movements believe. They have met in the Network on Economic Transformation (NOW). In this panel we want to discuss with you how we can work together to make the economy more solidary and how we can cooperate with other movements locally and globally.

---

***Time:*** 03:00-04:30 p.m.

***Event*** Needs not profits: Reshaping the whole of work.

Utopia 2048 - How will we organize work globally according to our needs?

***Speakers:*** Llanquiray Painemal, Gabriele Winker, Katrin Mohr and Cecosesola.

Audio channel for German translation: http://stream.foodada.de:8000/DE1

Audio channel for English translation: http://stream.foodada.de:8000/EN1

Audio channel for Spanish translation: http://stream.foodada.de:8000/ES1

***Description:*** There is no future in subordinating work - in all its forms - and the satisfaction of all human needs to growth and profit. The event will focus on ideas of work in a friendlier future, looking at similarities and differences. To this end, we also ask: Which steps of transformation are helpful, and how can we act together in solidarity and beyond national borders, whether in relationships, families or factories, in neighbourhoods or hospitals, daycare centres and schools, in offices or on fields?

---

***Time:*** 05:00-06:30 p.m.

***Event*** Global freedom of movement for all - from political demand to joint action.

***Speakers:*** Aisha Camara, Charles Heller, Alassane Dicko, Jane Wangari, Elizabeth Ngari and Katharina Morawek.
Audio channel for German translation: http://stream.foodada.de:8000/DE1

Audio channel for English translation: http://stream.foodada.de:8000/EN1

Audio channel for French translation: http://stream.foodada.de:8000/FR1

***Description:*** Global freedom of movement is known to many as a political demand. But often there is a lack of exchange about common understanding and collective action. The panel discussion will take up relevant utopias and provide insights into their potentials, limitations and contradictions. In addition, we will discuss possible transformation paths and strategies as well as necessary alliances. Let us empower ourselves: Let us make global freedom of movement conceivable and feasible for all!

---

***Time:*** 08:00-09.55 p.m.

***Event*** How to? Society of the Many - Democratic-dialogical thinking and new forms of interaction and communication

***Speakers:*** Charlotte Knorr, Rudaba Badakhshi, Antje Barten and Britta Borrego.

Audio channel for English translation: http://stream.foodada.de:8000/EN1

***Description:*** In this panel, people with different, individual perspectives on our society meet. What they have in common is their commitment to greater diversity and social participation. Together we want to present visions and changes for a society of the many, think aloud, explore and bring them together. What are the structures and practices of a society in which the democratic promise to treat each other as equals and free would be fulfilled for all?

### Friday, August 28th 

---

***Time:*** 09:30-10:30 a.m.

***Event*** Morning Start with Derya Binışık.

Audio channel for English translation: http://stream.foodada.de:8000/EN1

---

***Time:*** 11:00 a.m.-12:30 p.m.

***Event*** Reconsidering love.

***Speakers:*** Vivian Dittmar, Lann Hornscheidt, Tobi Rosswog, Peter Schönhöffer und Manuela Kuhar.
Audio channel for English translation: http://stream.foodada.de:8000/EN1

***Description:*** Our concepts of love and relationships, gender and sexuality are influenced by today's economic and societal power structures. These shape the dynamics of our relationships - and they often prevent true intimacy. How will our capacity to love increase in the future, and how can we change the way we see our relationships - to each other and to nature? And how will this therefore change the way our economy and society works?

---

***Time:*** 03:00-04:30 p.m.

***Event*** Social guarantees for everyone everywhere – visions and implementation ideas from feminist, union and globalization-critical viewpoints.

***Speakers:*** Renana Jhabvala, Ute Fischer, Renate Wapenhensch, Dagmar Paternoga, Hinrich Garms, Klaus Seitz, Michael Levedag and Daniela Gottschlich.

Audio channel for English translation: http://stream.foodada.de:8000/EN2

***Description:*** Social guarantees are financial, infrastructural and care-related safety measures which are available to every human everywhere. They should guarantee everyone's existence and participation in society. Which concrete visions can feminists, unions and globalization critics contribute? What steps do we need to take in order to implement them – in the short and long term? Who and what is blocking social guarantees? How to overcome those blockades? What national and international alliances are necessary?

---

***Time:*** 05:00-06:30 p.m.

***Event*** How do we move around (or not) in 2048?

***Speakers:*** Elias Bohun, Elisa Puga Cevallos, Christiane Benner and Janna Aljets.

Audio channel for German translation: http://stream.foodada.de:8000/DE1

Audio channel for English translation: http://stream.foodada.de:8000/EN1

Audio channel for Spanish translation: http://stream.foodada.de:8000/ES1

***Description:*** Which utopias regarding mobility do different people around the world hold? How does a just and ecological mobility for all look like? How do we live and move around in cities, what kind of mobility will there be in rural areas? How are the mobility needs of marginalised groups satisfied? How will the production of future transport modes be organised? How do we travel and explore the world? Activists, practitioners and researchers will join to develop utopias regarding mobility and strategies for transformation.

---

***Time:*** 08:00-09:45

***Event*** And how do we get to Utopia? Pathways of social-ecological Transformation
Pathways of socio-ecological transformation.

***Speakers:*** Elisabeth Jeglitzka, Uwe Meinhardt, Ceren Türkmen, Nilufer Koç und Linda Schneider.
Audio channel for English translation: http://stream.foodada.de:8000/EN1

***Description:*** To achieve a future for all by 2048, we need common answers to the following questions: How do we want to live? And how do we get there? How do we cope with diverging interests? Which political means do we consider suitable? What can we achieve with reforms and where do we need more? What can we learn from existing alternatives and groups, especially from the Global South? Here, we bring together different perspectives.

### Saturday, August 29th 

---

***Time:*** 09:30-10:30 a.m.

***Event*** Morning Start with Julia Fritzsche.

Audio channel for English Translation: http://stream.foodada.de:8000/EN1

---

***Time:*** 11:00 a.m.-12:30 p.m.

***Event*** Who shapes the Future of School? – Processes for implementing Global Learning carefully examined.

***Speakers:*** Jona Blum, Tina Schauer, Anne-Christin Tannhäuser, Mandy Singer-Brodowski und Nadine Golly.

Audio channel for English translation: http://stream.foodada.de:8000/EN1

***Description:*** Global Learning & Sustainability should be introduced as cross-cutting issues in schools - not only in the content but also in the didactics and in the design of the school as an institution. This is what (inter)national framework documents and processes seek to achieve - e.g. the German "Curriculum framework education for sustainable development". We discuss: How good are these framework processes and their implementation? What criticism is there and what perspectives are missing? Which structures and actors are blocking them? And: Do they point the way for an emancipatory, globally just school of the future?)

---

***Time:*** 03:00-04:30 p.m.

***Event*** Podium and Performance: Utopias in Arts and Culture (1/2) - Collective work as utopia.

***Speakers:*** AAA Collective, Jan Deck, Polymora.

Audio channel for German translation: http://stream.foodada.de:8000/DE1

***Description:*** In what way are Arts and Culture related to utopias? What role do they assume in social transformation processes, when developing utopias or strategies to resistance? How do our working methods, our interaction, our networks and our potential for action affect society?


---

***Time:*** 05:00-07:00 p.m.

***Event*** Podium and Performance: Utopias in Arts and Culture (2/2) - Art as a space for action

***Speakers:*** Senja Brütting, Elena Strempek, Sabiha Keyif, Tiara Roxanne und Ronya Othmann.

Audio channel for German translation: http://stream.foodada.de:8000/DE1

***Description:*** What role does art play in social transformation processes, in the development of utopias and strategies of resistance? Together with internationally active artists we will discuss challenges for artistic practice in the current social context as well as anti-racist, decolonial and anti-patriarchal strategies of artistic creation. Short performative interventions or lecture performances by the invited artists create shared sensual experiences and structure the podium.

---

***Time:*** 08:00-09:00 p.m.

***Event*** The trap - a play by Riadh Ben Ammar.

***Description:*** "We only sleep when the sun comes up ... then we are sure that the vampires will not come anymore," said Momo, a refugee living in Saxony. A theatre from Tangier to Pirna, about the closed EU borders and its misunderstandings .... fraudsters? Then let's talk about it! Not "Welcome to stay", but for freedom of movement ...

### Sunday, August 30th 

---

***Time:*** 09:30 a.m.-12.30 p.m.

***Event*** Congress Closure.
