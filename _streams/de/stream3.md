---
layout: stream
title:  Futur III
stream_date:
  begin: 2020-08-25 10:00:00 +02:00 # Time
  end:   2020-08-29 21:30:00 +02:00 # Time
svg: analog
---

## Willkommen im Saal Futur III
Auch hier erwartet euch ein vielfältiges Livestreamprogramm (siehe unter der "Leinwand"). Wir bedanken uns herzlich bei 
ecapio, dass sie uns für die Streams im Futur III ihren Youtube-Kanal zur Verfügung stellen.
### Nächste Veranstaltung
**29.08., 15:00-16:30: Podium - Internationale Finanzen und globale Gerechtigkeit - ein kurzer Blick auf Finanzen, koloniale Kontinuitäten und die Klimakrise**

<iframe class="board16-9" src="https://www.youtube.com/embed/Rb51npq6JBA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


Wenn ihr Übersetzung benötigt, klickt auf den für euch passenden Button und öffnet den Link in einem neuen Tab oder neuen Fenster. Der Übersetzungsstream und der Videostream können ggf. leichte zeitliche Verschiebungen aufweisen. Wir bitten um Nachsicht. Außerdem bitte nicht wundern: die Links funktionieren nur, wenn gerade übersetzt wird.
[Deutscher Stream](http://stream.foodada.de:8000/DE3){:.btn.centered}


## Programm

### Samstag, 29.08.

***Uhrzeit***: 15:00-16:30

***Veranstaltung***: Podium - Internationale Finanzen und globale Gerechtigkeit - ein kurzer Blick auf Finanzen, koloniale Kontinuitäten und die Klimakrise

***Vortragende***: Anne Löscher, Daniela Gabor, Ndongo Sylla

***Übersetzung***: DE

***Beschreibung***:
Die Frage, wie Klimagerechtigkeit, ein Ende von neokolonialen Beziehungen und Ungleichheit erreicht werden können, lässt sich nicht ohne einen Blick auf die Finanzsphäre zu werfen beantworten. In dem Panel wird es um jedes dieser Aspekte gehen. N'dongo Sylla spricht über eine panafrikanistische Alternative zum französischen Währungsimperialismus. Daniela Gabor gibt Einblicke in die scheinbar undurchsichtige Sphäre gegenwärtiger Finanzpraktiken. Anne Löscher beleuchtet den Zusammenhang zwischen dem internationale Finanzwesen und der Klimakrise in Ländern der Peripherie.

