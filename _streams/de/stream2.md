---
layout: stream
title:  Futur II
stream_date:
  begin: 2020-08-25 10:00:00 +02:00 # Time
  end:   2020-08-29 21:30:00 +02:00 # Time
svg: analog
---

## Willkommen im Saal Futur II
Auch hier erwartet euch ein vielfältiges Livestreamprogramm (siehe unter der "Leinwand"). Wir bedanken uns herzlich bei Freundeskreis Videoclips, dass sie uns für die Streams im Futur II ihren Youtube-Kanal zur Verfügung stellen.
### Nächste Veranstaltung
**26.08., 15:00-16:30: Podium - Digitalität zwischen Befreiung und Herrschaft**

<iframe class="board16-9" src="https://www.youtube.com/embed/yOyD1txerY0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


Wenn ihr Übersetzung benötigt, klickt auf den für euch passenden Button und öffnet den Link in einem neuen Tab oder neuen Fenster. Der Übersetzungsstream und der Videostream können ggf. leichte zeitliche Verschiebungen aufweisen. Wir bitten um Nachsicht. Außerdem bitte nicht wundern: die Links funktionieren nur, wenn gerade übersetzt wird.
[Deutscher Stream](http://stream.foodada.de:8000/DE2){:.btn.centered}
[English Stream](http://stream.foodada.de:8000/EN2){:.btn.centered}
[Español Stream](http://stream.foodada.de:8000/ES2){:.btn.centered}
[Portugues Stream](http://stream.foodada.de:8000/PT2){:.btn.centered}


## Programm

### Mittwoch, 26.08.

<hr />
***Uhrzeit***: 15:00-16:30

***Veranstaltung***: Podium - Digitalität zwischen Befreiung und Herrschaft

***Vortragende***: Andrea Vetter, Kris de Decker, Sabine Nuss, Natalia Messer

***Übersetzung/Translation***: D/EN/ES

***Beschreibung***

Die digitale Technik der Zukunft wird heute entwickelt – von wem und mit welchen Zielen? Von Träumen einer high-tech digitalen Planwirtschaft bis hin zu Visionen einer low-tech radikaldemokratischen Gesellschaft ohne Expertentum ist vieles möglich. Diese Vorstellungen sind nicht widerspruchsfrei: welche Form von Digitalität lässt sich sozial-ökologisch gerecht organisieren? Wo liegen Gefahren von Herrschaft, und welche Art der Befreiung können wir von Technik erwarten? Wir diskutieren verschiedene Ansätze und greifen historische Beispiele wie das Projekt Cybersin in Chile auf.


### Donnerstag, 27.08.

<hr />
***Uhrzeit***: 15:00–16:30

****Veranstaltung***: Institutionen der Freiheit und Solidarität? - Erkundungen jenseits von Markt und Staatsherrschaft

***Vortragende***: Simon Sutterluetti, Stefan Meretz, Racquel Gutierrez, Eva von Redecker

***Übersetzung/Translation***: D

***Beschreibung***
Die 'großen' Alternativen Kapitalismus und Staatssozialismus erwiesen sich als Spielarten von Naturzerstörung, Patriarchat, Rassismus und Lohnarbeit. Jenseits von ihnen stellt sich die Frage nach gesellschaftlicher Koordination neu: Was kann an die koordinierende Stelle von Staat und Markt treten? Wie produzieren und verteilen wir Güter und Dienstleistungen zur gerechten Befriedigung menschlicher Bedürfnisse? Wie treffen wir möglichst herrschaftsarm globale Entscheidungen? Ein Gespräch zwischen Kritischer Theorie, Feminismus, südamerikanischer Befreiungsbewegung und Commons.

<hr />
***Uhrzeit***: 17:00-18:30

***Veranstaltung***: Podium - Die Schule des Guten Lebens

***Vortragende***: Margret Rasfeld, Samira Ghandour, Ângela Biz Antunes, Rudaba Badakhshi, Franziska Pritzke, Peter Schulz, Vanessa de Oliveira Andreotti

***Übersetzung/Translation***: D/EN/ES

***Beschreibung***:

Wie sieht eine Schule im Jahr 2048 aus? Wie lernen wir und wie begegnen sich hier Menschen? Wie funktioniert hier Globales Lernen? Wer bestimmt über was? „Schule“ im Jahre 2048 wird ganz anders sein als heute. Klar ist: Sie wird sich an einem guten Leben für alle orientieren und: Sie unterstützt Menschen dabei, eine solidarische Lebensweise zu leben. Um ein Gefühl dafür zu bekommen, was das genau bedeuten kann, wollen wir einen Blick in die Zukunft wagen und durch verschiedene Brillen schauen.


### Samstag, 29.08.

<hr>
***Uhrzeit***: 15:00-16:30

***Veranstaltung***: Podium - Soziale Protestbewegungen – Meilensteine auf dem Weg zu globaler Gerechtigkeit

***Vortragende***: Dr. Juhi Tyagi, Dr. Esther Muinjangue, Limbert Sánchez Choque, Nathalie Nad-Abonji, Ferhad Ahma

***Übersetzung/Translation***: D/ES

***Beschreibung***:

Protestbewegungen gewinnen weltweit an Momentum. An den unterschiedlichsten Orten schließen sich Menschen zusammen, um für eine gerechte und solidarische Zukunft zu kämpfen. Innerhalb der Podiumsdiskussion soll eine globale Perspektive auf diese Protestbewegungen eingenommen werden. Wir wollen fragen, wie unterschiedliche Bewegungen weltweit voneinander lernen und profitieren können. Schlussendlich unter der Prämisse, dass diese dazu beitragen, dass die über Jahrhunderte bestehenden kolonialen Ungerechtigkeiten im Jahr 2048 ausgeglichen sind.
