---
layout: stream
title:  Futur I
stream_date:
  begin: 2020-08-25 10:00:00 +02:00 # Time
  end:   2020-08-29 21:30:00 +02:00 # Time
svg: analog
---
## Willkommen im Saal Futur I
Hier wird ein Großteil der Livestreams des Zukunft Für Alle Kongresses ausgestrahlt. Wir wünschen gute Unterhaltung und anregende Diskussionen.
### Nächste Veranstaltung
**25.08., 16:00-18:00: Kongressauftakt mit Bini Adamczak, Theater X, Kai Kuhnhenn und Nina Treu**

<iframe class="board16-9" src="https://www.youtube.com/embed/ZjSbOpK6ws4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Wenn ihr Übersetzung benötigt, klickt mit auf den für euch passenden Button und **öffnet den Link in einem neuen Tab** oder neuen Fenster. Der Übersetzungsstream und der Videostream können ggf. leichte zeitliche Verschiebungen aufweisen. Wir bitten um Nachsicht. Außerdem bitte nicht wundern: die Links funktionieren nur, wenn gerade übersetzt wird.


[Deutsches Audio](http://stream.foodada.de:8000/DE1){:.btn.centered}
[English audio](http://stream.foodada.de:8000/EN1){:.btn.centered}
[Audio Español](http://stream.foodada.de:8000/ES1){:.btn.centered}
[Audio Français](http://stream.foodada.de:8000/FR1){:.btn.centered}

## Programm

### Dienstag, 25.08.

---

***Uhrzeit***: 10:00

***Veranstaltung***: Pressekonferenz Zukunft Für Alle mit Ronja Morgenthaler, Zukunft Für Alle Pressesprecherin, Nina Treu, Zukunft Für Alle Projektkoordination, Nilda Inkermann, Zukunft Für Alle, Programm-AG und Moctar von Afrique-Europe Interact/Alarm Phone Sahara.

***Übersetzung/Translation***: EN

---

Uhrzeit: 16:00-18:05
***Veranstaltung***: Kongressauftakt.

***Vortragende***: Bini Adamczak, Theater X, Kai Kuhnhenn und Nina Treu.

***Übersetzung/Translation***: EN 

***Beschreibung***: Die politische Autorin und Künstlerin Bini Adamczak nähert sich dem Thema Utopien auf philosophische Weise und stellt sich die Frage: Wie Utopie? Danach stellen Kai Kuhnhenn und Nina Treu aus dem Konzeptwerk die wichtigsten Ergebnisse des 2019 initiierten Projekts „Zukunft für alle“ vor. 

---

***Uhrzeit***: 20:00-21:45

***Veranstaltung***: Eröffnungspodium: „Aus Krisen von heute die Zukunft entwickeln“.

***Vortragende***: Antje von Broock, Tonny Nowshin, Tina Röthig und Ruth Krohn

***Übersetzung/Translation***: EN

***Beschreibung***: Inmitten von Krisen und Alltagskämpfen stellt sich die Frage nach der Utopie. Wie könnte eine ökologische und gerechte Wirtschafts- und Lebensweise aussehen? Am Beispiel der Klimakrise und Corona-Pandemie diskutieren die Sprecher*innen über ihre Utopien und Lösungsansätze. Wie viel Umverteilung brauchen wir? Kann ein starker Green New Deal die schlimmsten Schäden des Klimawandels abwenden? Reicht es die Marktwirtschaft stärker sozial-ökologisch zu regulieren oder verhindert sie grundlegend eine klimagerechte Welt? Wie weitreichend müssen gesellschaftliche Veränderungen sein?

### Mittwoch, 26.08.

---

***Uhrzeit***: 09:30-10:30

***Veranstaltung***: Morgenauftakt mit Natasha A. Kelly.

***Übersetzung/Translation***: EN

***Beschreibung***: Wir starten gemeinsam in den Kongresstag. Es erwartet euch eine Mischung aus Organisatorischem, einem inhaltlichen Impuls und Austausch. Herzstück ist der Impulsvortrag, der uns Inspiration für den Tag mitgeben wird. Zu Gast sind: Dr. Natasha A. Kelly (Afrofuturismus), Julia Fritzsche (Journalistin) und Derya Binışık (Reproduktive Rechte und soziale Gerechtigkeit) und zwei Menschen aus der Kongress-Organisation. Sie präsentieren ihre Vision aus unterschiedlichen Positionen zu Utopie und Transformation – intersektional feministisch.

---

***Uhrzeit***: 11:00-12:30

***Veranstaltung***: Strangpodium Wohnen: „Wohnen 2048 – kein Markt, kein Staat, kein Mietvertrag“.

***Vortragende***: Christoph Struenke, Stefania Koller, Mary Lürtzing, Rouzbeh Taheri and Lina Hurlin.

***Übersetzung/Translation***: EN
Beschreibung: Wie sieht Wohnen in unseren Visionen im Jahr 2048 aus und wie sind wir dahin gekommen? Nach einem Austausch über die Utopien der Referierenden wollen wir konkreter diskutieren. Wie werden Wohnungen und Häuser 2048 finanziert und wie sind sie organisiert? Wie wird Wohnraum verteilt und wer entscheidet das? Wer stellt Wohnraum überhaupt zur Verfügung? Wie wird gebaut und entschieden, was wo wie gebaut wird? Und wer kümmert sich um die Instandhaltung des Wohnraums und der Häuser?

---

***Uhrzeit***: 15:00-16:30

***Veranstaltung***: Strangpodium Landwirtschaft und Ernährung: „Produktionsverhältnisse und Zugang zu Lebensmitteln in einem gerechten Ernährungssystem“.

***Vortragende***: Nora McKeon, Julianna Fehlinger, Aboubakar Soumahoro und Marie Populus.

***Übersetzung/Translation***: DE/EN/FR

***Beschreibung***: Wir diskutieren drei zusammenhängende Bereiche eines utopischen, gerechten Ernährungssystems: die Produktion, Verteilung und Steuerung – und fokussieren die soziale und gesellschaftliche Dimension. Wie sehen gute Arbeitsbedingungen aus und wie kommen wir dahin? Wie ist eine demokratische und partizipative Verteilung organisiert? Welche Politiken ermöglichen Ernährungssouveränität? Wir sprechen über ein utopisches System und diskutieren die Grundlagen mit drei Expertinnen und Praktikerinnen.

---

***Uhrzeit***: 17:00-18:30

***Veranstaltung***: Strangpodium Energie: „Energie für die Zukunft für alle“.

***Vortragende***: Tonny Nowshin, Linda Schneider, Nguy Thi Khanh und Lebogang Mulaisi.

***Übersetzung/Translation***: DE/EN

***Beschreibung***: Die Nutzung von Energie ist aufs Engste damit verknüpft, wie wir leben und produzieren. Gleichzeitig ist das Energiesystem maßgeblicher Treiber der Klimakrise, Ursache von Ausbeutung und Kriegen weltweit und schließt viele vom Zugang aus. Für den dringend notwendigen Umbau bleiben nur wenige Jahre. Wir wollen darüber diskutieren, wie das Energiesystem der Zukunft beschaffen sein müsste, damit es Klima und allen Menschen gleichermaßen gerecht wird - und wie der Weg dorthin gestaltet werden kann.

---

***Uhrzeit***: 20:00-21:45

***Veranstaltung***: Abendpodium: „Ökonomie der Zukunft – Wie organisieren wir demokratische und bedürfnisorientierte (Re-)Produktion?“.

***Vortragende***: Nina Treu, Nic Odenwälder, Simon Sutterluetti, Natalia Lizama L. und Andrea Vetter.

***Übersetzung/Translation***: EN

***Beschreibung***: Die globalen Krisen zeigen: Wir müssen uns auf Grundzüge einer Alternative zum Kapitalismus verständigen. Wie könnten wir Produktion, Reproduktion und Verteilung demokratisch und bedürfnisorientiert organisieren? Gibt es in der Wirtschaft 2048 noch einen Markt, oder mehr staatliche Planung? Wie könnte herrschaftsfreie globale Koordination aussehen? Müssen wir für unsere Existenz noch Lohnarbeit leisten oder basiert eine Utopie v.a. auf Motivation und Selbstauswahl? Wir versuchen uns hier gemeinsam Antworten zu nähern.

### Donnerstag, 27.08.

---

***Uhrzeit***: 09:30-10:30

***Veranstaltung***: Morgenauftakt mit Kate Čabanová und Werner Rätz.

***Übersetzung/Translation***: EN

---

***Uhrzeit***: 11:00-12:30

***Veranstaltung***: Strangpodium Solidarisches Wirtschaften: „Anders Wirtschaften! Das Netzwerk Ökonomischer Wandel“.

***Vortragende***: Silke Helfrich, Christian Felber, Matthias Schmelzer und Karin Walter.

***Übersetzung/Translation***: EN

***Beschreibung***: Brauchen alternative Ökonomien eine gemeinsame Strategie, um eine solidarische Wirtschaftsweise aufzubauen? Ja, glauben Vertreter*innen verschiedener alternativ-ökonomischer Bewegungen. Sie haben sich im Netzwerk Ökonomischer Wandel (NOW) zusammengeschlossen. In diesem wollen wir mit euch diskutieren, wie wir gemeinsam die Wirtschaft solidarischer gestalten und dabei mit anderen Bewegungen lokal und global zusammenarbeiten können.

---

***Uhrzeit***: 15:00-16:30

***Veranstaltung***: Strangpodium Die Ganze Arbeit: „Bedürfnisse statt Profite: Das Ganze der Arbeit neu gestalten“.

***Vortragende***: Llanquiray Painemal, Gabriele Winker, Katrin Mohr und Cecosesola

***Übersetzung/Translation***: DE/EN/ES

***Beschreibung***: Die gesellschaftliche Arbeit und die Befriedigung aller menschlicher Bedürfnisse Wachstum und Profit unterzuordnen, hat keine Zukunft. In der Veranstaltung geht es um Vorstellungen von Arbeit in einer freundlicheren Zukunft, mit ihren Gemeinsamkeiten und Unterschieden. Dafür fragen wir auch: Welche Schritte der Transformation sind hilfreich, und wie können wir miteinander solidarisch und über nationalstaatliche Grenzen hinaus schlagfertig handeln, egal ob in Beziehungen, Familien oder Fabriken, in Nachbarschaften oder Krankenhäusern, Kitas und Schulen, in Büros oder auf Äckern?

---

***Uhrzeit***: 17:00-18:30

***Veranstaltung***: Strangpodium Bewegungsfreiheit: „Globale Bewegungsfreiheit für alle – von der politischen Forderung zum gemeinsamen Handeln“.

***Vortragende***: Aisha Camara, Charles Heller, Alassane Dicko, Jane Wangari, Elizabeth Ngari und Katharina Morawek.

***Übersetzung/Translation***: DE/EN/FR1

***Beschreibung***: Globale Bewegungsfreiheit ist vielen als politische Forderung bekannt. Aber oftmals fehlt der Austausch über gemeinsame Verständnisse und kollektive Handlungsmöglichkeiten. Die Podiumsdiskussion greift dazu einschlägige Utopien auf und gibt Einblicke in deren Potentiale, Limitationen und Widersprüche. Zudem diskutieren wir mögliche Transformationspfade und -strategien sowie notwendige Allianzen. Ermächtigen wir uns: Lasst uns globale Bewegungsfreiheit für alle vorstellbar und machbar machen!

---

***Uhrzeit***: 20:10-21:55

***Veranstaltung***: Abendpodium: „How to? Gesellschaft der Vielen – demokratisch-dialogische Denkarbeit & neue Formen der Interaktion und Kommunikation“.

***Vortragende***: Charlotte Knorr, Rudaba Badakhshi, Antje Barten und Britta Borrego.

***Übersetzung/Translation***: EN

***Beschreibung***: Hier treffen sich Personen mit unterschiedlichen, individuellen Perspektiven auf unsere Gesellschaft. Sie verbindet, dass sie sich für mehr Diversität und gesellschaftliche Teilhabe einsetzen. Gemeinsam wollen wir Visionen und Veränderungen für eine Gesellschaft der Vielen vorstellen, laut denken, ergründen und zusammenführen. Was sind Strukturen und Praktiken einer Gesellschaft, in der sich das demokratische Versprechen, einander als Gleiche und Freie zu behandeln, für alle einlösen würde?

### Freitag, 28.08.

---

***Uhrzeit***: 09:30-10:30

***Veranstaltung***: Morgenauftakt mit Derya Binışık.

***Übersetzung/Translation***: EN

---

***Uhrzeit***: 11:00-12:30

***Veranstaltung***: Strangpodium Beziehungen: „Unbeherrscht lieben“.

***Vortragende***: Vivian Dittmar, Lann Hornscheidt, Tobi Rosswog, Peter Schönhöffer und Manuela Kuhar. 

***Übersetzung/Translation***: EN

***Beschreibung***: Unsere Vorstellungen von Liebe und Beziehungen, Geschlecht und Sexualität sind beeinflusst von heutigen wirtschaftlichen und gesellschaftlichen (Herrschafts-)Strukturen. Diese formen unsere Beziehungsdynamiken - und verhindern oft echte Nähe und Intimität. Wie können sich unsere Liebesfähigkeit, unsere Beziehungen miteinander und zur Natur in Zukunft verändern? Und inwiefern wird das auch seinerseits unser wirtschaftliches, gesellschaftliches und ökologisches Handeln verändern?

---

***Uhrzeit***: 15:00-16:30

***Veranstaltung***: Strangpodium Soziale Garantien: „Soziale Garantien für alle überall – feministische, gewerkschaftliche und globalisierungskritische Visionen und Umsetzung“.

***Vortragende***: Renana Jhabvala, Ute Fischer, Renate Wapenhensch, Dagmar Paternoga, Hinrich Garms, Klaus Seitz, Michael Levedag und Daniela Gottschlich.

***Übersetzung/Translation***: EN

***Beschreibung***: Soziale Garantien sind monetäre, infrastrukturelle und fürsorgerische soziale Absicherungen, die jedem Menschen überall zugänglich sind. Sie sollen allen die Existenz und gesellschaftliche Teilhabe garantieren. Welche konkreten Visionen gibt es dazu aus feministischer, gewerkschaftlicher und globalisierungskritischer Sicht? Welche Schritte zur Umsetzung sind nötig – kurz- und langfristig? Wer und was blockieren Soziale Garantien? Wie können diese Blockaden überwunden werden? Welche Bündnisse (national, international) sind nötig?

---

***Uhrzeit***: 17:00-18:30

***Veranstaltung***: Strangpodium Mobilität: „Mobilität im Jahre 2048?“.

***Vortragende***: Elias Bohun, Elisa Puga Cevallos, Christiane Benner und Janna Aljets. 

***Übersetzung/Translation***: DE/EN/ES

***Beschreibung***: Welche Mobilitätsutopien haben unterschiedlichste Menschen weltweit? Wie sieht eine gerechte und ökologische Mobilität für alle aus? Wie leben und bewegen wir uns in der Stadt, und wie sieht die Mobilität im ländlichen Raum aus? Wie werden die Mobilitätsbedürfnisse marginalisierter Gruppen befriedigt? Wie wird die Produktion der zukünftigen Verkehrsträger organisiert sein? Wie bereisen und erkunden wir die Welt? 
Aktivistinnen, Praktikerinnen und Wissenschaftler*innen werden gemeinsam Mobilitäts-Utopien und Transformations-Strategien entwickeln.

---

***Uhrzeit***: 20:00-21:45

***Veranstaltung***: Abendpodium: „Und wie kommen wir zur Utopie? – Wege der sozial-ökologischen Transformation“.

***Vortragende***: Elisabeth Jeglitzka, Uwe Meinhardt, Ceren Türkmen, Nilufer Koç und Linda Schneider.

***Übersetzung/Translation***: EN

***Beschreibung***: Um bis 2048 eine Zukunft für alle zu erreichen, brauchen wir gemeinsame Antworten folgende Fragen: Wie wollen wir leben? Und wie kommen wir dahin? Wie gehen wir mit unterschiedlichen Interessen um? Welche politischen Mittel sehen wir als geeignet an? Was können wir mit Reformen erreichen und wo braucht es mehr? Was können wir von bestehenden Alternativen und Akteuren, besonders aus dem Globalen Süden, lernen? Hier bringen wir verschiedene Perspektiven zusammen.

### Samstag, 29.08.

---

***Uhrzeit***: 09:30-10:30

***Veranstaltung***: Morgenauftakt mit Julia Fritzsche.

***Übersetzung/Translation***: EN

---

***Uhrzeit***: 11:00-12:30

***Veranstaltung***: Strangpodium Bildung (2): „Wer gestaltet die Zukunft der Schule? – Prozesse zur Umsetzung des Globales Lernens unter der Lupe“.
Vortragende: Jona Blum, Tina Schauer, Anne-Christin Tannhäuser, Mandy Singer-Brodowski und Nadine Golly.

***Übersetzung/Translation***: EN

***Beschreibung***: Globales Lernen & Nachhaltigkeit sollen als Querschnittsthemen in die Schulen - nicht nur in die Inhalte sondern auch in die Didaktik und bei der Gestaltung der Schule als Institution. So wollen es (inter)nationale Rahmendokumente und -prozesse wie etwa der Orientierungsrahmen Globale Entwicklung oder der Nationale Aktionsplan BNE. Wir diskutieren: Wie gut sind diese Rahmenprozesse und ihre Umsetzung? Was wurde schon geschafft? Welche Kritik gibt es? Welche Strukturen und Akteure blockieren sie? Und: Weisen sie den Weg für eine emanzipatorische global gerechte Schule der Zukunft?

---

***Uhrzeit***: 15:00-16:30

***Veranstaltung***: Strangpodium Art For Utopia (1/2): „Kollektives Arbeiten als Gesellschaftsutopie“.

***Vortragende***: AAA Collective, Jan Deck, Polymora.

***Übersetzung/Translation***: DE

***Beschreibung***: Wie hängen Kunst und Utopie zusammen? Wie wirken sich unsere Arbeitsweisen, unser Miteinander, unsere Netzwerke und unser Aktionspotential auf die Gesellschaft aus? Gemeinsam mit international agierenden Künstler*innen und Kollektiven erörtern wir alternative Formen des Arbeitens und der Vernetzung. Wie kann Kunst beim Unlearning, Undoing und Unteaching unterstützend wirken? Kurze performativen Interventionen bzw. Lecture Performances der eingeladenen Künstler*innen schaffen gemeinsame sinnliche Erfahrungen und strukturieren das Podium.

---

***Uhrzeit***: 17:00-19:00

***Veranstaltung***: Strangpodium Art For Utopia (2/2): „Kunst als Aktionsraum“.

***Vortragende***: Senja Brütting, Elena Strempek, Sabiha Keyif, Tiara Roxanne und Ronya Othmann.

***Übersetzung/Translation***: DE

***Beschreibung***: Welche Rolle spielt Kunst in gesellschaftlichen Transformationsprozessen, bei der Entwicklung von Utopien und von Strategien des Widerstands? Gemeinsam mit international agierenden Künstler*innen erörtern wir Herausforderungen für die künstlerische Praxis im aktuellen gesellschaftlichen Kontext sowie antirassistische, dekoloniale und antipatriachiale Strategien des Schaffens. Kurze performativen Interventionen bzw. Lecture Performances schaffen gemeinsame sinnliche Erfahrungen und strukturieren das Podium.

---

***Uhrzeit***: 20:00-21:00

***Veranstaltung***: Theaterstück: „Die Falle“ von Riadh Ben Ammar.

***Beschreibung***: Wir schlafen nur, wenn die Sonne aufgeht... dann sind wir sicher, dass die Vampire nicht mehr kommen, so Momo, Bewohner einer Geflüchtetenunterkunft in Sachsen. Ein Theater von Tanger nach Pirna, über die geschlossene EU-Außengrenze und ihre Missverständnisse.... Betrüger*innen? Dann lass uns darüber reden! Nicht "Welcome to stay", sondern für Bewegungsfreiheit...

### Sonntag, 30.08.

---

***Uhrzeit: 09:30-12:30***

***Veranstaltung***: Kongressabschluss.

