![Build Status](https://gitlab.com/pages/jekyll/badges/master/build.svg)
![Jekyll Version](https://img.shields.io/gem/v/jekyll.svg)

---

Digitale Karte des Kongresses „Zukunft für Alle“.

View it live at https://miguelsanmiguel.gitlab.io/zfa-karte

[Learn more about GitLab Pages](https://pages.gitlab.io) or read the the [official GitLab Pages documentation](https://docs.gitlab.com/ce/user/project/pages/).

## Help
- [Markdown Syntax](https://kramdown.gettalong.org/quickref.html)
- For **Youtube Iframes**, use an _embed_ link, and substitute `width="560" height="315"` with `class="board16-9"`
- Add `{:.btn}` to links to convert them into buttons (as in: `[text](url){:.btn}`)
- Keep the same names of pages (and spaces) in any language
- Create inner URLs in links simply with their filename (without extension), except for spaces and streams, where you must acknowledge that before (as in: `spaces/filename`)
- Accordion:
  >   - title: Beware proper indentation and...
  >   content: >
  >     Double any newline inside of the content.

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Getting Started](#getting-started)
  - [Start by forking this repository](#start-by-forking-this-repository)
  - [Start from a local Jekyll project](#start-from-a-local-jekyll-project)
- [GitLab CI](#gitlab-ci)
- [Using Jekyll locally](#using-jekyll-locally)
- [GitLab User or Group Pages](#gitlab-user-or-group-pages)
- [Did you fork this project?](#did-you-fork-this-project)
- [Other examples](#other-examples)
- [Troubleshooting](#troubleshooting)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Getting Started

You can get started with GitLab Pages using Jekyll easily by looking at https://gitlab.com/pages/jekyll.

**Remember you need to wait for your site to build before you will be able to see your changes**.  You can track the build on the **Pipelines** tab.

### Edit this repository online
Edit your website through GitLab. View the results under `http://miguelsanmiguel.gitlab.io/zfa-karte/`.

### As a local project
To work locally with this project, you'll have to clone the repository and push your changes. Follow the steps below:

1. Fork, clone or download this project
1. Install Ruby, Rubygems and Bundler if not present.
1. Download dependencies: `bundle install` (executed from the root directory of this project).
1. Build and preview changes under `http://localhost:4000/` with: `bundle exec jekyll serve  --config _config.yml,_config_dev.yml`.
1. Add content.
1. Push your repository and changes to GitLab.

## Formatting
For a Markdown cheatsheet, see `https://kramdown.gettalong.org/quickref.html`.

## Other examples

* [jekyll-branched](https://gitlab.com/pages/jekyll-branched) demonstrates how you can keep your GitLab Pages site in one branch and your project's source code in another.
* The [jekyll-themes](https://gitlab.com/groups/jekyll-themes) group contains a collection of example projects you can fork (like this one) having different visual styles.

<!-- ## Troubleshooting -->

[ci]: https://about.gitlab.com/gitlab-ci/
[Jekyll]: http://jekyllrb.com/
[install]: https://jekyllrb.com/docs/installation/
[documentation]: https://jekyllrb.com/docs/home/
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages
