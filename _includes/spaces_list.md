{% for space in site.spaces %}
  - ## [{{ space.title }}]({{ space.url | prepend: site.baseurl }}){: .space-link}
{% endfor %}

<!-- {% for space in site.spaces %}
  - <span class="space-meta">{{ space.date | date: "%b %-d, %Y" }}</span>
    ## [{{ space.title }}]({{ space.url | prepend: site.baseurl }}){: .space-link}
{% endfor %} -->
