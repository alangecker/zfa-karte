---
layout: space
title:  "Livestreaming events"
svg: events
---
We will not let Corona take away the enormous political potential of jointly experienced educational events! Despite the largely digital programme, we would like to encourage you to come together in offline rooms, discuss your ideas and develop them further together, network with other interested and active people and apply what you have learned in practice.

Streaming events will take place at various locations under Corona-appropriate conditions. Check the list below to find the nearest streaming location for you.

#### Augsburg (Nähe)
***Address:*** Hauptstraße 41, 86464 Emmersacker

***Date:*** 25.-30.8. We may stream all events possible. You are welcome to watch the congress, take a walk through the fields between the streams or help us keeping our perma culture.

***Programme:*** Alle [**Livestreams**](livestreams)

***Registration:*** [**Join this Telegram chat, please**](https://t.me/joinchat/RdkH9R18sQ-OM9EF8J2_Gg), [**contact**](mailto:k.jung@globalmatch.me)

***Accessibility:*** Depends on the streaming location. Contact Katharina for further information, please.

#### Berlin
***Address:*** 12099 Berlin, Oberlandstr. 26-35, BUFA-Gelände

***Date:*** 25.08., 19:00-22:30

***Programme:***[**Opening panel**](https://pretalx.com/zukunftfueralle/talk/8W9ECS/)

***Registration:*** [**Registration needed**](https://www.eventbrite.de/e/open-air-live-streaming-eroffnungspodium-zukunft-fur-alle-kongress-tickets-116902762309), [**contact**](mailto:tim@bufa.de)

***Accessibility:*** Very limitied, sorry.

#### Bielefeld
***Address:*** 33602 Bielefeld, Kulturhaus Bielefeld e.v., Werner-Bock-Str. 34c

***Date:*** 28.08., 20 Uhr

***Programme:*** “[**Und wie kommen wir zur Utopie? – Wege der sozial-ökologischen Transformation**](https://pretalx.com/zukunftfueralle/talk/DACVVR/)

***Registration:*** [**Per Mail**](mailto:carla.schmitz@t-online.de) 

***Barrierefreiheit:*** Yes

#### Braunschweig

Hosted by KuK-BS e.V., schrill e.V. und das Sonnensystem - more information to come

#### Bremen
***Address:*** 28199 Bremen, Westerstr. 58, KlimaWerkStadt

***Date:*** Donnerstag, 27.08. + Freitag 28., 19:45-22:15

***Programme:*** [**Evening panel Thursday**](https://pretalx.com/zukunftfueralle/talk/37KQD8/) + [**Evening panel Friday**](https://pretalx.com/zukunftfueralle/talk/DACVVR/)

***Registration:*** [**Register here**](mailto:anmeldung@klimawerkstadt-bremen.de)

***Barrierefreiheit:*** Partially (Toilets no)

#### Freiburg
***Address:*** 79100 Freiburg, Haus des Engagements, Rehlingstraße 9

***Date:*** 26.08., 20:00-22:00

***Programme:*** [**Evening panel: Ökonomie der Zukunft**](https://pretalx.com/zukunftfueralle/talk/N8YVHP/)

***Registration:*** [**Stadtwandler**](mailto:info@stadtwandler.org)

***Barrierefreiheit:*** No.

#### Hannover
***Address:*** Kötnerholzweg 13, 30451 Hannover, im Hinterhof  

***Date:*** 25.08.-30.08., 20:00-22:00

***Programme:*** [**All evening panels and the theatre on the closing evening**](https://pretalx.com/zukunftfueralle/)

***Registration:*** [**Contact**](info@endboss.eu)

***Barrierefreiheit:*** Partially (Toilets no)

#### Leipzig \#1 (Ost)
***Address:*** Gartenprojekt am Bagger, Kiebitzstraße, 04349 Leipzig

***Date:*** 25.08., 16:00 und 25.08., 20:00

***Programme:*** [**Kick-Off**](https://pretalx.com/zukunftfueralle/talk/WTYHHT/), [**Opening panel: "Aus Krisen von heute die Zukunft entwickeln”**](https://pretalx.com/zukunftfueralle/talk/8W9ECS/)

***Registration:*** Anmeldung verpflichtend und telefonisch unter 01788601973

***Accessibility:*** Tricky outdoor underground therefore only partially accessible.

#### Leipzig \#2 (Westen)
***Address:*** Stadtgarten H17, Hähnelstr. 17, 04177 Leipzig-Lindenau; in case of bad weather conditions check here: [**Facebook-Events here**](https://www.facebook.com/NFJLeipzig/) and [**here**](https://www.facebook.com/Stadtgarten-H17-1551403855088744/) or [**here**](http://www.freiraumsyndikat.de/?p=termine) and [**here**](https://naturfreundejugend-leipzig.de/).

***Date:*** 25.08.-30.08., 20:00-22:00

***Programme:*** [**Alle Abendpodien sowie das Theater zum Abschluss**](https://pretalx.com/zukunftfueralle/)

***Registration:*** Not needed, but due to Corona restrictions the number of visitors is limited. Sorry.

***Accessibility:*** Tricky outdoor underground therefore only partially accessible (toilet not at all)

#### München
***Address:*** 80336 München, Schwanthalerstraße 80, EineWeltHaus München: Dachterrasse (bei schönem Wetter)/Raum 211 (bei schlechtem Wetter)

***Date:*** 26.08., 20:00-22:00 (Einlass 19:30)

***Programme:*** [**Abendpodium: Ökonomie der Zukunft**](https://pretalx.com/zukunftfueralle/talk/N8YVHP/)

***Registration:*** [**Anmeldung bitte hier**](anmeldung@klimaherbst.de)

***Accessibility:*** Yes

#### Rheinland (Lützerath)
***Address:*** Mahnwache Lützerath an der Landstraße 277 (in 41812 Erkelenz)

***Date:*** 25.08. und 28.08., ab 20:00

***Programme:*** [**Opening panel**](https://pretalx.com/zukunftfueralle/talk/8W9ECS/) and [**Evening panel on Friday**](https://pretalx.com/zukunftfueralle/talk/DACVVR/) 

***Registration:*** No. In case of questions, contact the vigil in Lützerath, please: 0152 01339091

***Accessebility:*** Ja

#### Komplex Schwerin
***Address:*** Pfaffenstr. 4, 19055 Schwerin

***Date:*** 26.08. 20 Uhr

***Programme:*** [**Evening panel Ökonomie der Zukunft**](https://pretalx.com/zukunftfueralle/talk/N8YVHP/)

***Registration:*** [**info@bundjugend-mv.de**](mailto:info@bundjugend-mv.de)

***Barrierefreiheit:*** No

#### Taucha, OT Sehlis
***Address:*** 04425 Sehlis, An der Schmiede 4

***Datum:*** 25.08., 26.06., 27.08.

***Programme:*** [**Abendpodien**](http://www.schmiede4.net/?q=ZukunftFuerAlle)

***Registration:*** [**Werkstatt für nachhaltiges Leben und Arbeiten e.V.**](mailto:wfnlua@posteo.de)

***Accessibility:*** Event location yes, toilets no