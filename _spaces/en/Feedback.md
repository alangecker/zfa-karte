---
layout: space
title:  "Feedback"
svg: feedback
---

We greatly appreciate your feedback on the atmosphere, content and technology of the congress. Write to us anonymously via our contact form. Just [**follow this link**](urlhttps://zukunftfueralle.jetzt/kontakt/#feedback). If you wish to get an answer, leave us your e-mail-address within the feedback message, please.

