---
layout: space
title:  "Welcome"
svg: welcome
---
## Welcome to the Congress Future for All!

How do we live at 2048? And how do we get there? This question is at the heart of our congress and its workshops, panel discussions, diverse cultural events, bar tables and the open exhange spaces. We will develop and experiment with transformative ideas; debate, plan and lay out new paths of societal change. We will connect with one another over sectors and boundaries and learn from each other's utopic perceptions.

Participation at
* [**workshops**](workshops),
* [**open space**](open-spaces) and
* [**networking events**](synergieraum)

is for registered users only. Registration was open until August 20 and will be re-opened for spontanteous registration from Tuesday, August 25 on under anmeldung.zukunftfueralle.jetzt.However, there are several public events, online and offline. For more information, check

* [**Public livestreams**](livestreams)
* [**Offline events**](livestreaming-events)

### Infopoint
If you are registered, you can turn to the digital infopoint chat for general questions. It can be reached via the conference platform venueless under "Info point", listed on the left-hand side. To see it, you have to click on the little compass and then enter the room ("beitreten"). 

If you are not registered, please write to [**zfa-checkin@knoe.org**](mailto:zfa-checkin@knoe.org) for general inquiries which are not answered in the Congress manual.

## Background

The congress has been prepared in a broad and open organising circle. Institutional funders and private donors support Future For All ([**more information here**](https://zukunftfueralle.jetzt/en/about-us/donations/#institutionalfunders). For further important facts [**see the congress manual**](https://zukunftfueralle.jetzt/en/congress/congress-manual/).

An extensive brochure on the insights of this congress has been published within the project Future for All, coordinated by the [**Konzeptwerk Neue Ökonomie**](https://konzeptwerk-neue-oekonomie.org/english/). You can [**find the publication here**](https://zukunftfueralle.jetzt/buch-zum-kongress/). Unfortunately it's only available in German.

[Workshop Area](https://kongressgelaende.zukunftfueralle.jetzt/){:.btn.centered target="_parent"}

Now we would like to wish you inspiring and fruitful time on our congress space!

