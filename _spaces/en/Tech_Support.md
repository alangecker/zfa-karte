---
layout: space
title:  "Technical support"
svg: support
---
Hello and welcome!

We are here to solve any technical problems that may arise during your stay at the congress as quickly as possible. Since each of us can only help one person at a time, our resources are limited. That's why we've uploaded a number of guides on this page, which may help you to improve your technical congress experiences by yourself:

* [**Tech-Host Manual**](https://zukunftfueralle.jetzt/wp-content/uploads/2020/08/Techhost-Anleitung_eng.pdf) PDF
* [**Big Blue Button user guide**](https://zukunftfueralle.jetzt/wp-content/uploads/2020/08/Big-Blue-button-for-participants-and-tips-for-pleasant-video-conferences.pdf) PDF
* [**Guidelines for participants**](https://zukunftfueralle.jetzt/wp-content/uploads/2020/08/Guidelines-for-Participants.pdf) PDF

If that doesn't help, feel free to contact us via a support chat in venueless or call us at +4934165673750. Due to capacity reasons, we can only process the individual problems of registered participants. Sorry.
