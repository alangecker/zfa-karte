---
layout: space
title:  "Public livestreams"
svg: public
---

Welcome to the Future For All livestreaming center (a.k.a. *ZFA-TV*). We broadcast the Opening and Closing of the Congress as well as all morning- and evening panels, all the thematic panels and some arts and culture events live. All livestreams will be translated in English and sign language. Registration is not required.

May the panels provide you with deep insights into different social debates, encourage further discussions and support us all with the development of shared utopias. If you don't feel like watching the panels on your own, you may find the information about our livestreaming events in your area [here](livestreaming_events). You can also organize one yourself! (more information [**here**](https://zukunftfueralle.jetzt/kongress/streaming-veranstaltungen/), in German only)

---

[Futur I](streams/stream1){:.btn.centered}
[Futur II](streams/stream2){:.btn.centered}
[Futur III](streams/stream3){:.btn.centered}

Below there is an overview on all livestreams. If you would like to get English information on the livestream programme, click on the buttons above, please. There you will find the programme for each channel and the information on translations.

## Programme

|Date|Time|Event|Livestream|
|------|------|------|------|
|25.08.|10:00|Pressekonferenz Zukunft Für Alle. Zugang ab 09:45. Mehr Infos **[hier](https://zukunftfueralle.jetzt/presse/)**.|[**Futur I**](streams/stream1)|
|25.08.|16:00-17:50|**[Kongressauftakt](https://pretalx.com/zukunftfueralle/talk/WTYHHT/)**.|[**Futur I**](streams/stream1)|
|25.08.|20:00-21:45|**[Eröffnungspodium: "Aus Krisen von heute die Zukunft entwickeln"](https://pretalx.com/zukunftfueralle/talk/8W9ECS/)**.|[**Futur I**](streams/stream1)|
|26.08.|09:30-10:30|**[Morgenauftakt mit Natasha A. Kelly](https://pretalx.com/zukunftfueralle/talk/YCL78A/)**.|[**Futur I**](streams/stream1)|
|26.08.|11:00-12:30|**[Strangpodium Wohnen: "Wohnen 2048 – kein Markt, kein Staat, kein Mietvertrag"](https://pretalx.com/zukunftfueralle/talk/RKKPZW/)**.|[**Futur I**](streams/stream1)|
|26.08.|15:00-16:30|**[Strangpodium Landwirtschaft und Ernährung: "Produktionsverhältnisse und Zugang zu Lebensmitteln in einem gerechten Ernährungssystem"](https://pretalx.com/zukunftfueralle/talk/9EHTJP/)**.|[**Futur I**](streams/stream1)|
|26.08.|15:00-16:30|**[Strangpodium Digitalität: "Digitalität zwischen Befreiung und Herrschaft "](https://pretalx.com/zukunftfueralle/talk/WGFLAQ/)**.|[**Futur II**](streams/stream2)|
|26.08.|17:00-18:30|**[Strangpodium Energie: "Energie für die Zukunft für alle"](https://pretalx.com/zukunftfueralle/talk/ENE88Y/)**.|[**Futur I**](streams/stream1)|
|26.08.|20:00-21:45|**[Abendpodium: "Ökonomie der Zukunft - Wie organisieren wir demokratische und bedürfnisorientierte (Re-)Produktion?"](https://pretalx.com/zukunftfueralle/talk/N8YVHP/)**.|[**Futur I**](streams/stream1)|
|27.08.|09:30-10:30|**[Morgenauftakt mit [tba]](https://pretalx.com/zukunftfueralle/talk/7GAQQC/)**.|[**Futur I**](streams/stream1)|
|27.08.|11:00-12:30|**[Stranpodium Solidarisches Wirtschaften: "Anders Wirtschaften! Das Netzwerk Ökonomischer Wandel"](https://pretalx.com/zukunftfueralle/talk/F8XG7C/)**.|[**Futur I**](streams/stream1)|
|27.08.|15:00-16:30|**[Strangpodium die ganze Arbeit: "Bedürfnisse statt Profite: Das Ganze der Arbeit neu gestalten"](https://pretalx.com/zukunftfueralle/talk/89KJKE/)**.|[**Futur I**](streams/stream1)|
|27.08.|15:00-16:30|**[Strangpodium Gesellschaftsorganisation: "Institutionen der Freiheit und Solidarität? - Erkundungen jenseits von Markt und Staatsherrschaft"](https://pretalx.com/zukunftfueralle/talk/A838YP/)**.|[**Futur II**](streams/stream2)|
|27.08.|17:00-18:30|**[Strangpodium Bewegungsfreiheit: "Globale Bewegungsfreiheit für alle – von der politischen Forderung zum gemeinsamen Handeln"](https://pretalx.com/zukunftfueralle/talk/3FBP3L/)**.|[**Futur I**](streams/stream1)|
|27.08.|17:00-18:30|**[Strangpodium Bildung (1): "Die Schule des Guten Lebens"](https://pretalx.com/zukunftfueralle/talk/WJYKJB/)**.|[**Futur II**](streams/stream2)|
|27.08.|20:10-21:55|**[Abendpodium: "How to? Gesellschaft der Vielen - demokratisch-dialogische Denkarbeit &amp; neue Formen der Interaktion und Kommunikation"](https://pretalx.com/zukunftfueralle/talk/37KQD8/)**.|[**Futur I**](streams/stream1)|
|28.08.|09:30-10:30|**[Morgenauftakt mit Derya Binışık.](https://pretalx.com/zukunftfueralle/talk/B9L3XF/)**.|[**Futur I**](streams/stream1)|
|28.08.|11:00-12:30|**[Strangpodium Beziehungen: "Unbeherrscht lieben"](https://pretalx.com/zukunftfueralle/talk/XNNWM3/)**.|[**Futur I**](streams/stream1)|
|28.08.|15:00-16:30|**[Strangpodium Soziale Garantien: "Soziale Garantien für alle überall – feministische, gewerkschaftliche und globalisierungskritische Visionen und Umsetzung"](https://pretalx.com/zukunftfueralle/talk/7ZLGFM/)**.|[**Futur I**](streams/stream1)|
|28.08.|17:00-18:30|**[Strangpodium Mobilität: "Mobilität im Jahre 2048?"](https://pretalx.com/zukunftfueralle/talk/LYFEWL/)**.|[**Futur I**](streams/podium1)|
|28.08.|20:00-21:45|**[Abendpodium: "Und wie kommen wir zur Utopie? - Wege der sozial-ökologischen Transformation"](https://pretalx.com/zukunftfueralle/talk/DACVVR/)**.|[**Futur I**](streams/stream1)|
|29.08.|09:30-10:30|**[Morgenauftakt mit Julia Fritzsche](https://pretalx.com/zukunftfueralle/talk/8A3QYR/)**.|[**Futur I**](streams/stream1)|
|29.08.|11:00-12:30|**[Strangpodium Bildung (2): "Wer gestaltet die Zukunft der Schule? – Prozesse zur Umsetzung des Globales Lernens unter der Lupe"](https://pretalx.com/zukunftfueralle/talk/YRLTYS/)**.|[**Futur I**](streams/stream1)|
|29.08.|15:00-16:30|**[Strangpodium Art For Utopia (1/2): "Kollektives Arbeiten als Gesellschaftsutopie"](https://pretalx.com/zukunftfueralle/talk/DJ8VFF/)**.|[**Futur I**](streams/stream1)|
|29.08.|15:00-16:30|**[Strangpodium Globale Gerechtigkeit: "Soziale Protestbewegungen – Meilensteine auf dem Weg zu globaler Gerechtigkeit"](https://pretalx.com/zukunftfueralle/talk/YJSXUJ/)**.|[**Futur II**](streams/stream2)|
|29.08.|15:00-16:30|**[Strangpodium Finanzsystem: [tba]](https://pretalx.com/zukunftfueralle/talk/VDEWHD/)**.|[**Futur III**](streams/stream3)|
|29.08.|16:45-18:30|**[Strangpodium Arts For Utopia (2/2): "Kunst als Aktionsraum"](https://pretalx.com/zukunftfueralle/talk/YM3TYE/)**.|[**Futur I**](streams/stream1)|
|29.08.|20:00-21:00|**[Theater: "Die Falle"](https://pretalx.com/zukunftfueralle/talk/YW9QHY/)**.|[**Futur I**](streams/stream1)|
|30.08.|09:30-12:30|**[Kongressabschluss](https://pretalx.com/zukunftfueralle/talk/3BQNFV/)**.|[**Futur I**](streams/stream1)|

