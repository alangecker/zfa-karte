---
layout: space
title:  "Workshops"
svg: workshops
---

[Enter workshop area](https://kongressgelaende.zukunftfueralle.jetzt/){:.btn.centered target="_parent"}

The workshops are a central element of our learning. Within the fifteen thematic tracks we discuss specific questions on the just and ecological Future for All (for more information [**click here**](https://zukunftfueralle.jetzt/wp-content/uploads/2020/04/Track_Information.pdf)). The workshops bring us to into a discussion, help us to draft utopias and design ways to make them reality.

You can exchange on yours and others workshops everyday on several occassions. After lunch there is time for your [**affinity group**](bezugsgruppen). Later in the afternoon, we connect the dots of all workshops to sound utopias within "[**Connecting utopias!**](synergiestrang)". [**Open spaces**](open-spaces) give room for *your* topics and formats. You may as well join open spaces that are organised by other participants, of course. The [**BIPoC**](bipoc-space) and [**FLINT\***](flint-space) Safer Spaces offer exchange on congress and other matters. After all, the [**bar**](bar) is open all day, but we recommend especially in the evenings. There will be space for chats and discussions on anything.

The second round of registration has ended. You can still [**register for the congress**](https://contacts.knoe.org/content/registration-future-all) as a whole, but not for workshops. Then you can apply for workshops that are not fully booked yet. This application form will be online on Tuesday evening [**right here**](https://anmeldung.zukunftfueralle.jetzt/). Currently 2/3 of the workshop seats are taken.

[Enter workshop area](https://kongressgelaende.zukunftfueralle.jetzt/){:.btn.centered target="_parent"}