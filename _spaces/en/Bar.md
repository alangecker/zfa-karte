---
layout: space
title:  'Online Bar "The Future"'
svg: bar
accordion:
  - title: Introduction
    content: >
      Have you ever had a stimulating discussion or an intimate conversation in a video conference room with a hundred participants? No? Of course. The Future was created to make small rounds possible despite the spatial distance, but still not to lose touch with the "unknown others". It is a construct of several (virtual) rooms through which you can move and – if you stay a little longer – even have to. Because the rule is: Just as you move through a bar or a café in your non-virtual life, you will do so in The Future. If you have no appointments, the courtyard is a good place to arrive. Thirsty? Before you go to your real fridge (or wherever), you leave the virtual space you are in and go to the bar (via a mouse click). Experience shows that a lot of people gather here. You can stay here or sit down with your drink in the sofa corner; or just have a look at what's going on in the theme rooms. And also important: If you have to go to the toilet, don't just stand up in front of your camera, but change to the toilet and switch your camera off.


      As in non-virtual life, you will never be in more than one room at a time. This way everyone else in the respective rooms knows exactly whether you are there or not. So nobody talks to you when you are away and you give others the opportunity to talk about things you are not interested in. By strolling through the bar rooms, new constellations of conversation can always arise and you get to know new people. And you always have an excuse to leave a round and look for new spaces with conversation partners if, for example, you don't like the topic at the moment. You are sceptical that the concept really works? Trust me: After a few hours it feels very natural.


      One more thing: Since the physical distance threatens to cool down all human interactions, just treat everyone present like old friends. It makes many things easier and nicer!
  - title: Preparations and rules
    content: >
      You do not need to register anywhere in The Future to be a part of it. Nevertheless, there are a few things you should be aware of:

      * Please use the browser **Chrome/Chromium** when you go to the pub. The software Jitsi has a problem with Firefox/Safari and the connection can be much worse for everyone if you don't use Chrome/Chromium.
      
      * Be aware to change rooms as you would do in non-virtual life. It's best to leave this page open, open the link of the room in a new tab, close the tab when you leave it and move from here to the next room.
      
      * Be sure to wear a **headset or headphones**. Since you will be able to listen to music together in The Future, anything else will cause terrible feedback. Please stick to that.
      
      * **Leave your microphone on**. Unlike in workshops and conferences, laughing/munching/smoking should be heard and contributes to the atmosphere. It is also much more relaxed to talk if you don't have to turn the microphone on and off all the time.
      
      * Please be aware of the **rules of the individual rooms**. The rules are framework conditions through which The Future will come alive.

      The Future is **self-organized**, independent of venueless and has no individual hosts. Only if people (you could be one of them!) get actively involved, e.g. by spreading the common music channels or bringing suggestions for discussion, the future can become alive.
  - title: Main rooms
    content: >
      The Future was built in an old building. You enter it through a rotten wooden gate, walk down a musty-smelling hallway with far too many bicycles parked there and finally enter the place where everyone always comes together: The courtyard. Pallet benches, seat cushions, shady places for the day and chains of lanterns for the night. A large room for everyone.


      Directly opposite the entrance you will see a makeshift standing counter where many people quickly fetch their drinks. Right next to that counter you find the seat counter for all those who are tired of getting up again and again for every drink. In case of rain or if a smaller group just wants to retreat, there is a covered sofa corner. And in the end, so that nobody dares to disturb the idyll, the way to the toilet is not far either. In the lockable cabins it is also possible to go to the toilet together.


      * [**Inner courtyard**](https://meet.ffmuc.net/Z-Innenhof)
      
      * [**Standing counter**](https://meet.ffmuc.net/Z-Innenhof)

      * [**Seating counter**](https://meet.ffmuc.net/Z-Sitztresen)
      
      * [**Covered sofa corner**](https://meet.ffmuc.net/Z-Sofaecke)

      * [**Pinboard**](https://pad.elevate.at/p/zukunft-pinnwand) --> You can find the pinboard at the bottom of this site, too.


      **restrooms**
      
      * [**Toilet**](https://meet.ffmuc.net/Z-Toilette)
      
      * [**Cabin 1**](https://meet.ffmuc.net/Z-Kabine1)

      * [**Cabin 2**](https://meet.ffmuc.net/Z-Kabine2)

      * [**Cabin 3**](https://meet.ffmuc.net/Z-Kabine3)
    
      
      *Technical note: You can lock a room (e.g. the cabin) by clicking on the sign at the bottom (security options) and then setting a password. The password expires once the room is completely empty.*
  - title: Theme rooms
    content: >
      In pairs of two, two strands of topics of the congress share a room. For some reason nobody wanted to make common cause with the financial system. By connecting the congress topics with virtual rooms, it should be possible to talk about them directly after the lectures/workshops in The Future and the same participants can get together again more quickly. Apart from that, all rooms are always and by everyone freely usable. Some rooms (the debating club and the dance floor in particular) have special rules and we kindly ask you to respect them. Each of the rooms also has a side area. This can help, for example, if you are meeting in a room that is already full or if you would rather meet in private. For your privacy, you are always welcome to set a password for the side rooms.


      * **Debating Club** (Education: School.Power.Future & All the Work)

        The foremost room on the first floor was obviously once used as a classroom and is still in good condition. The blackboard has been wiped, the tables have been moved to the side and a large circle of chairs has been placed in the middle. The room is therefore particularly suitable for discussions of all kinds. To make sure that these do not happen by accident, there is an extra area on the Pinboard where you can freely write down what you would like to discuss (and how). No matter if it's a debate about content or your own ideas for a better world: Describe your point briefly on the pinboard, suggest several date options and wait if there are people interested. If such a discussion date has been set, the room should be kept free for it.


        [**Debating Club**](https://meet.ffmuc.net/Z-Debattierclub)
        
        [**Side Table**](https://meet.ffmuc.net/Z-Debattierclub-2)        
        
        [**Pinboard**](https://pad.elevate.at/p/zukunft-pinnwand)


      * **Planning office with canvas** (Social Organization & Digitality)

        Also on the first floor there is a dusty planning office, where important decisions were probably made in earlier times. It seems that since then several groups have tried to give the room a new direction. But as no agreement could be reached, it became a better storage room. During the last days of preparation for the Future For All congress, some people have installed a video projector, hung up a sheet and organized small film evenings at the end of the day. So while it is often rather impolite to simply show funny youtube videos to other people, the planning office is perfect to lean back together. If you don't want to leave the common media consumption to chance, you can of course write suggestions on the pinboard.


        [**Planning office**](https://meet.ffmuc.net/Z-Planungsb)
        
        [**Side couch**](https://meet.ffmuc.net/Z-Planungsb-2)
        
        [**Pinboard**](https://pad.elevate.at/p/zukunft-pinnwand)
        
        
        *Technical note: youtube videos can be fed directly into Jitsi (at the three points). When a video is played, the microphones are automatically muted - just press and hold spacebar to talk.*


      * **Kubrick's Milk Bar** (Housing & Art and Culture)

        It's hard to believe, but someone seems to have actually furnished a room on the second floor of The Future that also looks like The Future. Obviously, a great Kubrick connoisseur has recreated vast amounts of props from "2001: Odyssey in Space" and "Clockwork Orange" to create a unique living space. White furniture and indirect light frame her collection, which she dubbed "Kubrick's Milk Bar". The big advantage: The Milk Bar doesn't just serve milk and whoever is here doesn't have to go to the counter in the inner courtyard. But just bumping into this room because of the drinks, is not appropriate and should be avoided at all costs.


        [**Kubrick's milk bar**](https://meet.osna.social/Z-Kubricks-Milchbar)
        
        [**Milk bar counter**](https://meet.osna.social/Z-Kubricks-Milchbar-2)


      * **Pillow Lounge** (The World of Relationships & Social Guarantees)

        It's warm here. It's soft here. It's cozy here, very cozy. No table is higher than the knee, no place exists where there are not tens of cushions within easy reach. This is a good place. A comfortable place. A place where it should also be possible to say uncomfortable things and reveal feelings.


        [**Pillow-Lounge**](https://meet.freifunk-aachen.de/Z-Kissen-Lounge)

        [**Mattress Corner**](https://meet.freifunk-aachen.de/Z-Kissen-Lounge-2)


      * **At the solar panels** (Energy and Climate & Global Justice)

        The roof of the building can be entered through a small hatch and if you come here in time you will experience the most beautiful sunsets. Between solar panels and brick chimneys, a day can be reflected excellently.


        [**At the solar panels**](https://meet.freifunk-aachen.de/Z-Solarpanels)
        
        [**next to it**](https://meet.freifunk-aachen.de/Z-Solarpanels-2)


      * **The Tower** (Financial System)
      
        The tower looks high, but on the way up it seems to be twice as high. Finally arriving at the highest level, the tower is considerably more run-down than expected: Every single spot of the room has been smeared with graffiti and all graffiti consists only of numbers. Numbers, numbers, numbers everywhere. Numbers above you, numbers to your right, numbers to your left and numbers in your nose - everywhere just numbers. And when you look from the tower, you see numbers streaking across the courtyard. Or are they people? It's hard from up here to tell the difference.
      
      
        [**The tower**](https://meet.ffmuc.net/Z-Turm)
      
        [**Bench**](https://meet.ffmuc.net/Z-Turm-2)


      * **Dance floor** (Freedom of Movement for All & Grassroots Mobility)
      
        Directly out of the courtyard a stony staircase leads down into the darkness and sometimes at night you can clearly hear bass from it. As improbable as it sounds, there have already been magical moments when someone suggested to go dancing virtually at late hours. And then the music was synchronized, the light in each room was dimmed, the microphones were switched off and for hours they danced together, despite being in their own room. It might sound strange: it was magic! Who will dare to open the dance floor at this congress?
        
        
        [**Dance floor**](https://meet.ffmuc.net/Z-Tanzflaeche)
        
        [**Dark corner**](https://meet.ffmuc.net/Z-Tanzflaeche-2)
        
        
        Important note: If you are here at night – i.e. you don't meet after a workshop to discuss – please make sure that your own room is darkened when dancing, to create an appropriate atmosphere. Make sure to turn off your microphone and leave it off. Conversations will be conducted via the chat. If someone is using the microphone all the time, this person may be escorted out of the room (the first person to enter the room can do this). Otherwise: Have fun here!


      * **The Table** (Agriculture and Food & Solidarity-based Economy)
        
        A small corridor leads out of the courtyard to a large vegetable patch, next to which a long table with white tablecloth is set up. If there are enough for everyone and everybody has access to them, then it is a desirable task to watch the pumpkins grow. This is exactly possible here. If you don't like to eat alone, you can meet here for lunch (between 12:30 and 14:00), coffee (16:30 - 17:00) or dinner (18:30-20:00) or just join in to find out what the others have prepared for you.
        
        
        [**Table by the vegetable patch**](https://meet.ffmuc.net/Z-Tafel)

        [**Next table**](https://meet.ffmuc.net/Z-Tafel-2)
  - title: Music
    content: >
      The Future is decentralized and self-organized. The online tool [[sync-tube]](https://sync-tube.de/) allows you to listen to youtube songs together. However, in order for more people to experience the same music with you ( something of utmost importance for the real feeling of a pub!) you have to ensure that the links to the sync-tube rooms are spread. And since no one knows who' s in which sync-tube room or who is listening to music at all, you should follow a simple rule: Whenever you enter a room, copy the link to your sync-tube room into the chat window of Jitsi. Without making a big fuss about it, others can then just listen in and decide whether they want to stay in that channel themselves and then spread the word accordingly.
      
      So in short: You want to listen to music with others?

      
      * Create a room on [[sync-tube.de]](https://sync-tube.de/) or enter an existing room via a link.
      
      
      * Search for songs on youtube and copy them into the sync-tube room. Unfortunately you cannot search for songs on sync-tube itself.
      
      
      * Always share your sync-tube link without being asked when you enter a room in The Future.


      This way, different "radio stations" can circulate in a well-stocked bar. You can also write your sync-tube links on the [pinboard](https://pad.elevate.at/p/zukunft-pinnwand), allowing more people to listen to your music. Once a sync-tube channel is empty, it is no longer accessible and a new channel must be opened.
  - title: Tips and tricks
    content: >
      * What are the **opening hours** of The Future? The Future is open 24 hours a day. However, the highest probability to meet someone is in the evening.
      
      
      * **Why do I rarely meet someone?** If everyone just clicks through the rooms and doesn't stay anywhere, no one will ever meet anyone. If you don't have a date, it is therefore recommendable to stay alone in one place for a longer time. The counter and the inner courtyard are the places to go.
      
      For example, I would like to wait for others in the debating club, but I want to talk while I'm waiting. Is that possible? Yes. Technically you can be in two different rooms using two tabs. If you are explicitly waiting for something or someone, you can do so. Wait in one room with your microphone and camera turned off, while you are actively sitting at the counter, for example, chatting with others for so long. But try to keep it as short as possible and follow the rule of never being active in two rooms at the same time. If you go to the toilet, for example, you have to leave your active room and not just turn off the camera. Give others the opportunity to talk to each other without the feeling that someone might be listening to them.
    
    
       * **Jitsi is overloaded - what to do?** First, check if someone is using a browser other than Chrome/Chromium. Other browsers often use three times the bandwidth. If this is not the case, set video quality to low or switch off the camera completely. Better no picture than eternal discussions about bad technology. And last possibility: Move together to a room running on a different server. The following rooms have alternative servers to Freifunk Munich: "Kubricks Milchbar" (Chaostreff Osnabrück), "Kissen-Lounge" and "At the Solar Panels" (Freifunk Aachen)
     
     
      * **Camera stuck - what to do?** If you see the same person all the time, i. e. the camera does not switch between the speakers, go to the split view icon (at the bottom of the bar) and from there back again – problem solved.
     
     
      * **How can I talk to someone privately?** If you want to have such a conversation, you can protect a room with a password. Just click on the shield icon in the bottom bar and set the password. The password expires once the room is empty.
     
     
      * **How can I text something to someone in private?** You want to discuss something with someone without everyone knowing? For example, which room do you want to escape to in order to escape the current conversation? Use the private message function by clicking on the three dots on the corresponding icon of the participants.
     
     
       * I have an appointment with someone in room [xy], but **there are other people there - what to do?** Just try to take the side area of the corresponding room. If no one is there either, you can find each other through the pinboard.
     
     
       * I have another **problem**, a **suggestion** for improvement, or another **request**. **Where can I go?** Once the future is set, it's a self-organized place, and no one from the organising team is directly assigned to it. Write your request on the pinboard and look there again and again to see what is bothering others or which suggestions for improvement they have. If this does not help, you can contact the technical team [Link].
     
     
      * A cold winter is approaching - **is there an online pub for after the congress**? The [*Schwarzer Krauser*](https://pad.elevate.at/p/schwarzer-krauser) is self-organised and open all year round. And who knows, maybe The Future will stay?!
---

## Welcome to The Future!

As we can barely sit next to each other at the moment, the best thing to do is to escape to The Future. We think: there are worse things to do! For one week The Future is your social space to hang out with your people from all over the world and to meet people you like between events. Or The Future is simply the place where you can stand at the counter in the evening and spend the night with other congress participants. The big bonus: The Future is open for you 24 hours a day! The big problem: You have to bring your own drinks.

On your first arrival in the future, we recommend to take the drink of your choice and read the following explanations of the bar concept. In this sense: have a good time, cheers!

{% include accordion.html %}

**Here's a list of all tables and rooms of The Future:**

* [**At the solar panels**](https://meet.freifunk-aachen.de/Z-Solarpanels)
* [**Bench**](https://meet.ffmuc.net/Z-Turm-2)
* [**Cabin 1**](https://meet.ffmuc.net/Z-Kabine1)
* [**Cabin 2**](https://meet.ffmuc.net/Z-Kabine2)
* [**Cabin 3**](https://meet.ffmuc.net/Z-Kabine3)
* [**Covered sofa corner**](https://meet.ffmuc.net/Z-Sofaecke)
* [**Dark Corner**](https://meet.ffmuc.net/Z-Tanzflaeche-2)
* [**Dance floor**](https://meet.ffmuc.net/Z-Tanzflaeche)
* [**Debating club**](https://meet.ffmuc.net/Z-Debattierclub)
* [**Debating club side table**](https://meet.ffmuc.net/Z-Debattierclub-2)
* [**Inner courtyard**](https://meet.ffmuc.net/Z-Innenhof)
* [**Kubrick's Milk Bar**](https://meet.osna.social/Z-Kubricks-Milchbar)
* [**Kubrick's Milk Bar Counter**](https://meet.osna.social/Z-Kubricks-Milchbar-2)
* [**Mattress corner**](https://meet.freifunk-aachen.de/Z-Kissen-Lounge-2)
* [**Next to the solar panels**](https://meet.freifunk-aachen.de/Z-Solarpanels-2)
* [**Pillow lounge**](https://meet.freifunk-aachen.de/Z-Kissen-Lounge)
* [**Pinboard**](https://pad.elevate.at/p/zukunft-pinnwand)
* [**Planning office**](https://meet.ffmuc.net/Z-Planungsb)
* [**Planning office Couch**](https://meet.ffmuc.net/Z-Planungsb-2)
* [**Sitting counter**](https://meet.ffmuc.net/Z-Sitztresen)
* [**Standing counter**](https://meet.ffmuc.net/Z-Innenhof)
* [**Table by the vegetable patch**](https://meet.ffmuc.net/Z-Tafel)
* [**Table next to the table by the vegetable patch**](https://meet.ffmuc.net/Z-Tafel-2)
* [**Toilet**](https://meet.ffmuc.net/Z-Toilette)
* [**Tower**](https://meet.ffmuc.net/Z-Turm)    

#### Pinboard
<iframe class="board3-2" name="embed_readwrite" src="https://pad.elevate.at/p/zukunft-pinnwand?showControls=true&showChat=true&showLineNumbers=false&useMonospaceFont=false"></iframe>
