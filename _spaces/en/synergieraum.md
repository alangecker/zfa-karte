---
layout: space
title:  "Connecting utopias!"
svg: utopias
---
We will bring together perspectives and results - connect utopias! - from all congress events from Wednesday till Friday, from 5 p.m. onwards. We reach beyond the workshop horizon and enrich our utopias with their various contents. 

Missed the application deadline? Then become a member of our congress observation team. You can visit all your workshops and share your thoughts and visions on congress closure on sunday. In case there are still free places for "Connecting utopias", feel free to also join one of the slots. 

If you are interested in the congress observation, send us an [**E-Mail**](mailto:zfa-programm@riseup.net).
