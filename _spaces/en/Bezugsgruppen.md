---
layout: space
title:  "Affinity groups"
svg: groups
---

The affinity group meetings are digital, random meetings. They aim to allow diverse exchange with other participants of random choice. If you have signed up for an affinity group at the congress registration, we will randomly assign you to a group of up to seven people. Each group will then be meet in a discussion room in our congress software. This offer is reserved for people who have registered for the congress.

The affinity group rooms are open daily from 2 p.m. to 2:30 p.m. You can return to your room each day and continue your exchange over the congress. There will also be small discussion suggestions from the preparation group which you can use as inspiration for the discussion. But in the end it is you who decides what your meetings are about. If you get into deep discussions and need more time, you are welcome to take your talks to the [**Open Space**](open_spaces) and continue them there.