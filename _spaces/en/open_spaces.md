---
layout: space
title:  "Open space"
svg: open
---
You would like to dvelwe deeper into your topic? Or perhaps focus on something, which has not yet been covered by our programme adequately? Or you simply cannot wait to continue with an exciting debate that had just sparkled elswhere - this is your place to be! We offer daily Open Spaces from Wednesday to Saturday, 5 p.m. to 6:30 p.m.

If you would like to use the Open Space, please send us an email latest at 7:30 p.m. the previous day to [**zfa-openspace@riseup.net**](mailto:zfa-openspace@riseup.net) with all necessary information (see below). If we should receive more requests for an Open Space than we can fullfill on one day, we will meet a selection, so please suggest us an alternative day for your request. We behold the right to reject requests for spaces, which violate the main principles of the congress (based on our [**Compass for Utopias**](https://zukunftfueralle.jetzt/wp-content/uploads/2020/08/Zukunft_fuer_alle_Kompass.pdf), in German only :/).

Every morning around 9 am we will publish the Open Space programme for the day (including the corresponding rooms) at the Open Space Board at the bottom of this page.

The Open Space events will take place in the digital bar [**„The Future“**](bar). There you can simply enter the room of the event of your choice. An overview of the allocation of topics and rooms can be found in the pad on the site of the bar. Each room is for a maximum number of 15 people only; but there are rooms as a fallback option, if it gets too full.

[Go to Bar/Open Spaces](bar){:.btn.centered}

Your request for an Open Space slot via the email [**zfa-openspace@riseup.net**](mailto:zfa-openspace@riseup.net) should contain following information:

* Title and summary of the topic offered
* Purpose of the offer (exchange/discussion regarding…; input/workshop on…; networking for…)
* Day you want it to take place (+ possibly alternatives)
* Language of the event 
* Particular needs

