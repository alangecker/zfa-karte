---
layout: space
title:  Media center
svg: media
---

Unfortunately, we cannot show the great variety of analogue book tables and library here. But we would not like to do without additional material. On this page we provide you with music, videos and reading material that can accompany you through the congress. Especially for formats of art and culture you will find some important files here.

Workshop material for congress participants is [**stored here**](https://desk.knoe.org/index.php/s/98nkY7QeS57oYwo).

In the following you will first find the different media for the [**art and culture track**](art_for_utopia) with all the corresponding information. After that you will find some links and documents for further reading from the project [**"Future For All"**](https://konzeptwerk-neue-oekonomie.org/themen/zukunftfueralle/). At the end of the page there are several documents from the preparation process of the congress available for download.

### Art for Utopia

[Art for Utopia Programme](https://zukunftfueralle.jetzt/wp-content/uploads/2020/08/programm-download_klein_2.pdf){:.btn.centered}

#### Power Poetry, writing workshop

**Friederike Preuschen**

Video in German
<iframe width="560" height="315" src="https://www.youtube.com/embed/Aiag7jxu_ws" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

The video is part of Rike's workshop but can also be watched and realized independently. 

The writing workshop opens a space to experience empowering, reflective and resistant qualities of writing. Find a place where you would like to write for this. With this video you will be guided to write experimental poetry. It is about an intuitive, playful approach to language beyond the purely cognitive and without conventions. Random associations and humor are welcome. Utopian visions of the future or socio-political issues that currently occupy you will find expression here in your own personal way. If you like, you can present your Power Poem later in analogue or digital form.
 

#### Audiotour: Imponderabilien

**Kathrin Dröppelmann, Manuel Schwiers und Luka Lenzin**

Audio play in German

<a href="/assets/media/IMPONDERABILIEN.mp3" target="_blank">**Link to audio play**</a>

You can choose the way you want to move and the starting point autonomously. You only need headphones and a player with access to the media library.

Everything is there already. I discover a place with everything I always have with me - my ears, my feeling, my memory, my ideas, my energy, my time. On the 70-minute audio tour, guides from various perspectives animate people to actively perceive and combine the (only) seemingly unconnected. Philosophical inputs and perception exercises on the topic of memory refer to each other, sometimes playfully, sometimes poetically. If a memory lives in a place, do I always participate in it when I return there? And if my memory is linked to a time, do I carry the time around with me? Contrasts between individual sensual experience and audio track are welcome. The experience will be different for everyone - it cannot be repeated but tried several times, it cannot be argued about but communicated.

#### Apokalypse Yesterday - the series - in 4 episodes!

**A NeXt Generation Ensemble production.**
[www.theater-x.de/nextgeneration](http://www.theater-x.de/nextgeneration)

The future... how is it? Is it tired of everyone talking about her and expecting a lot from it? "The future will fix it!", "F*** utopias!" says NeXt Generation, cause what does the future actually do? It sits on the shoulders of those who today are fighting for a just present. It walks alongside those who already know stories of the apocalypse because they are already experiencing them. This is the world we live in. Before and during the pandemic. Today and long ago. What about tomorrow?


*Script/Concept/Performance*: Next Generation Ensemble mit Cevahir Kan, David Thiery, Joshiv Puni, Luisa Schönpflug, Nele Becker, Sam., Serina Secici, Tyrese Brodöhl, Ismail Metin & Rami El

*Director*: Annika Füser

*Daramaturgy*: Gwen Lesmeister

*Camera*: Felipe Frozza

*Post production*: Gwen Lesmeister & Annika Füser

*Music & Sound*: Nils Erhard

*Costumes crew*: Lua Argüello, Gugui Pellegrino, Pi Pintos, Saida Saad, Jesu Gor & Selina Thylmann

##### Folge 1: The future in the shisha bar.
Fire, water, fine dust! The apocalypse is upon us, although long warned of. But the people who have the power to change things just keep doing what they do... they can afford it. But what does the future hold?

<iframe width="560" height="315" src="https://www.youtube.com/embed/jHt9dms1ztA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


##### FOLGE 2: The miracles of technology
"With new technologies we can rid the world of its problems!" But who is producing this technology and under what conditions? Surveillance, the war machine, greed... who uses technology and for which purpose?

What does the future hold?

<iframe width="560" height="315" src="https://www.youtube.com/embed/Grsz0-UpaFU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


##### FOLGE 3: Wishlist
If we are all unique and individual, we can already fulfill our dreams. But what happens outside the glitter bubble?
<iframe width="560" height="315" src="https://www.youtube.com/embed/RogKa8ZJqo8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


##### Folge 4: Side by Side
This is what the future really looks like: It walks alongside those who are already fighting for the present. It sits on the shoulders of those whose very existence means resistance.

<iframe width="560" height="315" src="https://www.youtube.com/embed/ikF3LfE1xdQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

**About Theater X**
Theater X is a young community theater in Berlin-Moabit that is run jointly by young people and staff. At the centre of the stage programme and its own productions is a critical artistic examination of social conditions from the perspective of marginalised young people.
**[https://theater-x.com/](https://theater-x.com)**
**[https://theater-x.com/uberuns](https://theater-x.com/uberuns)**

#### Audio play: The Humus Revolution - State of Work

**Frieder Zimmermann**

Audio play in German

<a href="/assets/media/humusrevolution_as_MASTER.mp3" target="_blank">**Link to audio play**</a>

The audio feature can be combined with the workshop of the same name on Wednesday. Or you can listen to it independently.

In 2050, after several financial crises, catastrophic droughts and famines in Europe, people decide to question not only the political status quo but also the economic system. The central issue here is agriculture and its relationship to the soil. The audio feature reports from a possible future. Interviews from the ecological community of Schloss Tempelhof and Klein Jasedow in the Lasaaner Winkel area paint a hopeful picture of a not so distant future that can become real if the right decisions are made now.

#### Audio play: 2048 - Scenes from a world of tomorrow

**Margareta Steinrücke, Elisabeth Voß, Thomas Pfaff, Walter Groeh, Gerhard Klas, Ulrich Biermann**

Audio play in German

An online event for radio play listening and discussion will take place on **Wednesday evening at 20:00**.

What will the world look like in 2048 - if things go well? First-person narrators report from their lives: A different world, in solidarity, caring for people and nature. After a short introduction, we listen to a 45-minute podcast from the future together. Afterwards the team of the Audio-Utopistas and the Attac-Mini-AG Utopia would like to discuss with you: What do we need in order to get out of the alternative bubble, to make many people want a different future and to take away fears?

### Documents for the congress
- **[Congress manual](https://zukunftfueralle.jetzt/en/congress/congress-manual/)**
- **[Call for contributions](https://zukunftfueralle.jetzt/wp-content/uploads/2020/04/CALL_Future-for-all-1.pdf)** PDF
- **[Information on thematic tracks](https://zukunftfueralle.jetzt/wp-content/uploads/2020/04/Track_Information.pdf)** PDF
- **[Registration and submission guidelines](https://zukunftfueralle.jetzt/wp-content/uploads/2020/04/Guideline_Submission_Workshopdesign-1.pdf)** PDF

### How-Tos
- **[Big Blue Button](https://zukunftfueralle.jetzt/wp-content/uploads/2020/08/Big-Blue-button-for-participants-and-tips-for-pleasant-video-conferences.pdf)** PDF
- **[Guidelines for participants](https://zukunftfueralle.jetzt/wp-content/uploads/2020/08/Guidelines-for-Participants.pdf)** PDF
- **[Tech-Host Tutorial](https://zukunftfueralle.jetzt/wp-content/uploads/2020/08/Techhost-Anleitung_eng.pdf)** PDF
