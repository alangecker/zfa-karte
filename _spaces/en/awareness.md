---
layout: space
title:  "Awareness space"
svg: awareness
---
Welcome to the awareness page of the congress! This is the right place for you if you have a concrete request or if you want to inform yourself.

#### Awareness concept

Check out the Future For All congress awareness concept. Just click on this [link](https://zukunftfueralle.jetzt/en/awareness/) to view it in your browser. Or click here for a [PDF download](https://zukunftfueralle.jetzt/wp-content/uploads/2020/08/Awareness_Future-for-All.pdf).

#### Availability of the awareness team

There are several ways to get in contact with us. Which ever way you prefer: we will find a person to talk to for you. This might take a little while. Please select a communication channel that feels good good/appropriate for *you*.

* Phone: +4915735594971
* [Video call](https://kongressgelaende.zukunftfueralle.jetzt/rooms/15593140-00c7-47f0-8106-8d1b20d9d6c4){: target="_parent"}
* E-Mail: [zfa-awareness@riseup.net](mailto:zfa-awareness@riseup.net)
* Come by at the office (9h-21h on every congress day at Klingenstr. 22, Leipzig)
* You will get access to the BIPoC space and/or the FLINT\* space by contacting us via the above mentioned channels

Due to a lack of capacities not every event will be attended by a person from the awareness team. Generally, tech hosts can be contacted so they pass on your contact to the awareness team.
