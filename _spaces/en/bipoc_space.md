---
layout: space
title:  "BIPoC Space"
svg: bipoc
---
"BIPoC" has emerged as a self-description within the US-based civil rights movement and stands for "Black, Indigenous and People of Color". BIPoC does not describe biological characteristics but a social construction. The term positions itself against attempts of division with racism and culturalisation as well as against discriminating descriptions by the white majority society. The BIPoC Safer & Empowerment Space is open to all people who regard themselves as BIPoC.

What connects BIPoC are shared experiences of racism, othering and exclusion from the white-dominated majority society. The Safer & Empowerment Space pursues the goal of creating retreat spaces at a white-dominated congress in a white-dominated society in which we feel "safe" and understood; spaces in which we can share experiences with and empower one another, where we can retreat to, build bonds and resist,

[Go to BIPoC Safer Space](https://kongressgelaende.zukunftfueralle.jetzt/rooms/0ccf7802-3e68-49f2-bcfb-8c4521b1212d){:.btn.centered target="_parent"}

---

tuesday

* 7 - 8pm: Island of Healing. We can talk about our feelings and experiences at the congress as well as exchange impressions about it.

---

wednesday

* 1 - 2pm: Open Space. In this space you can talk about everything: about ideas for a transformative society, about own perspectives, about a visited workshop, or whatever you want- bring your stories & thoughts!
* 7 - 8pm: Island of Healing. We can talk about our feelings and experiences at the congress as well as exchange impressions about it.

---

thursday

* 1 - 2pm: Open Space. In this space you can talk about everything: about ideas for a transformative society, about own perspectives, about a visited workshop, or whatever you want- bring your stories & thoughts!

---

friday

* 6 - 7.30pm: Writing workshop on utopian societies. In this writing workshop we want to present our ideas for an intersectional and solidarian society in writings. Those who wish can share their own text at the end. We want to exchange our views again in the healing island.

---

saturday

* 1 - 2pm: Open Space. In this space you can talk about everything: about ideas for a transformative society, about own perspectives, about a visited workshop, or whatever you want- bring your stories & thoughts!
