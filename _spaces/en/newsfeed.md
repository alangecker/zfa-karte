---
layout: space
title:  Newsfeed
svg: ticker
---

<!-- <p class="rss-subscribe">subscribe <a href="{{ "/feed.xml" | prepend: site.baseurl }}">via RSS</a></p> -->

This is your morning cup of coffee to get a good start for each day and stay informed throughout the whole congress. We will share relevant technical updates and give the hottest tips to the daily program.

[**Registration still open**](https://contacts.knoe.org/content/registration-future-all)

[**Free workshop seats**](https://anmeldung.zukunftfueralle.jetzt/)

---

*25.08., 15:50* **++ The congress kicks off! ++**

Let’s go! The congress kicks off with inspiring talks by Bini Adamczak, Nina Treu und Kai Kuhnhenn. Watch the whole event in our livestream venue [**Futur I**](streams/stream1). In case you missed registration for workshops: no worries. You can still [**register here**](https://contacts.knoe.org/content/registration-future-all). The only difference: you cannot apply for any workshop you want. 2/3 of workshop spots are taken already and from this evening onwars we will offer all free workshop seats [**right here**](https://anmeldung.zukunftfueralle.jetzt/).

We are looking forward to an inspirational congress and six days addressing the questions: how do we want to live in 2048? And how do we get there?

P.S.: Today's opening panel starts at eight. It will be livestreamed at your [**favourite livestream**](streams/stream1) and there will, of course, be translation.


---
*August, 24th, late* **++ One more night and then we'll kick off the congress ++**

After a very long day the "Zukunftszentrale" ("Future's head office") is set to get us all on the way tomorrow. The first congress will start with the press conference at 10am. At 4 pm we will welcome all of you via livestream, followed by a keynote from Bini Adamczak (4.30pm) and the opening panel (8pm). [**All streams will be available on channel Futur I.**](streams/stream1)

---

*August, 20th, 10:30 am.* **++ The congress map is online ++**<br>

Dear world, last night the congress map popped up on our website. Good news: it will stay there! We only need to add a few links and information, make everything available for the public and – voilà! – the congress can kick off.

Looking forward to seeing you soon.

---
