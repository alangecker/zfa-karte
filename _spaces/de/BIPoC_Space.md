---
layout: space
title:  "BIPoC Safer Space"
svg: bipoc
---

"BIPoC" ist eine Selbstbezeichnung aus der us-amerikanischen Bürgerrechtsbewegung und steht für "Black, Indigenous and People of Color" (Schwarz, Indigen und Personen of Color). BIPoC beschreibt nicht biologische Merkmale, sondern eine soziale Konstruktion. Der Begriff positioniert sich gegen Spaltungsversuche durch Rassismus und Kulturalisierung sowie gegen diskriminierende Fremdbezeichnungen durch die weiße Mehrheitsgesellschaft. Nicht alle Menschen, die als nicht-weiß gelesen werden, sind automatisch BIPoC-Personen.Der BIPoC Safer & Empowerment Space ist offen für alle Menschen, die sich als BIPoC verstehen.

Was BIPoC miteinander verbindet, sind geteilte Rassismuserfahrungen, Ausgrenzung von der weiß dominierten Mehrheitsgesellschaft und kollektive Zuschreibungen des „Andersseins“ (auch: Othering). Der Safer & Empowerment Space verfolgt das Ziel, auf einem weiß-dominierten Kongress in einer weiß-dominierten Gesellschaft Rückzugsräume zu schaffen, in denen wir uns „sicher“ fühlen und verstanden fühlen. Räume, in welchen wir uns zurückziehen und vernetzen können, uns gegenseitig empowern und Erfahrungen miteinander teilen und uns widerständig halten können.

[zum BIPoC S&E Space](https://kongressgelaende.zukunftfueralle.jetzt/rooms/0ccf7802-3e68-49f2-bcfb-8c4521b1212d){:.btn.centered target="_parent"}

<hr>

### Programm

#### Dienstag:
*19-20 Uhr:* Erholungsinsel. Hier können wir uns über unsere Gefühle, Erfahrungen undEindrücke auf dem Kongress austauschen.

#### Mittwoch:
*13:00 - 14:00 Uhr:* Open Space. In diesem Space kann über alles geredet werden: über Ideen für eine transformative Gesellschaft, über eigene Perspektiven, über einen besuchten Workshop, oder oder oder - bringt gerne eure Geschichten & Gedanken mit!

*19:00 - 20:00 Uhr:* Erholungsinsel. Hier können wir uns über unsere Gefühle, Erfahrungen undEindrücke auf dem Kongress austauschen.

#### Donnerstag:
*13:00 - 14:00 Uhr:* Open Space. In diesem Space kann über alles geredet werden: über Ideen für eine transformative Gesellschaft, über eigene Perspektiven, über einen besuchten Workshop, oder oder oder - bringt gerne eure Geschichten & Gedanken mit!

#### Freitag:
*18:00 - 19:30 Uhr:* Schreibworkshop zu Utopischen Gesellschaften. In diesem Schreibworkshop wollen wir unsere Ideen für eine intersektionale und solidarische Gesellschaft schriftlich verfassen. Wer möchte, kann am Ende den eigenen Text teilen. In der Erholungsinsel wollen wir uns dann wieder austauschen.

#### Samstag:
*13:00 - 14:00 Uhr:* Open Space. In diesem Space kann über alles geredet werden: über Ideen für eine transformative Gesellschaft, über eigene Perspektiven, über einen besuchten Workshop, oder oder oder - bringt gerne eure Geschichten & Gedanken mit!
