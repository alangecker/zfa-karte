---
layout: space
title:  "Open Space"
svg: open
---
Ihr möchtet ein Thema vertiefen? Ihr wollt den Fokus auf eine Thematik legen, die eurer Ansicht nach im Programm noch nicht ausreichend beleuchtet wird? Oder ihr müsst eine anregende Diskussion fortführen, die sich an anderer Stelle entwickelt hat? Genau dafür ist der Open Space da. **Mittwoch bis Samstag bieten wir täglich von 17:00 bis 18:30 Uhr Open Spaces an.**

Wenn ihr einen Open Space gestalten möchtet, schickt uns bitte spätestens bis 19:30 am Vortag eine Mail an zfa-openspace@riseup.net [Link] mit allen notwendigen Informationen (siehe unten). Sollten an einem Tag mehr Open Space Vorschläge gemacht werden als verfügbar sind, treffen wir eine Auswahl. Wir bitten euch deshalb, mögliche Ausweichtage vorzuschlagen. Zudem behalten wir uns vor, Angebote abzulehnen, die der grundsätzlichen Ausrichtung des Kongresses entgegenstehen ([**zur Orientierung**](https://zukunftfueralle.jetzt/wp-content/uploads/2020/08/Zukunft_fuer_alle_Kompass.pdf)).

Jeden Morgen ab 09:00 Uhr veröffentlichen wir das Open Space Programm des Tages am Open Space Brett am Ende dieser Seite. Die Open Space-Veranstaltungen finden dann in der [**digitalen Bar „Die Zukunft“**](bar) statt. Dort könnt ihr einfach dem Raum beitreten, in dem die Veranstaltung eurer Wahl stattfindet. Eine Übersicht über die Zuteilung von Themen und Räumen findet ihr auch direkt im Pad auf der Seite der Bar (ganz unten auf der Barseite). Die Räume in der Bar sind je für max. 15 Personen, sollte es zu voll werden, könnt ihr noch Ausweich-Barräume nutzen.


[Zu den Open Spaces](bar){:.btn}

Deine Open Space Anmeldung bei [**zfa-openspace@riseup.net**](zfa-openspace@riseup.net) sollte Folgendes beinhalten:

* Titel und Kurzbeschreibung zum Thema des Angebots
* Anliegen des Angebots (Austausch/Diskussion zu…; Input/Workshop zu…; Vernetzung zu…)
* Tag an dem das Angebot stattfinden soll (+ evtl. Ausweichoption)
* Sprache des Angebots
* maximale Teilnehmer\*innen-Anzahl
* spezielle Bedarfe


|Raum und Link|Veranstaltungsinfos|Sprache|
| :------------- | :----------: | -----------: |
|[Open Space 1](https://kongressgelaende.zukunftfueralle.jetzt/){: target="_parent"}|**Veranstaltungstitel**, kurze Beschreibung was da alles so gecshieht und worum es eigentlich ganz, ganz genau genommen geht, damit es auch alle recht schnell verstehen, ob das nun was für sie ist oder eben nicht. Sollte nicht mehr als zwei Sätze lang sein und kann gerne mit [**Links**](https://de.wikipedia.org/wiki/Hyperlink) arbeiten. *maximale Teilnehmer\*innen-Zahl und andere Teilnahme- und Ausschlusskriterien*|English|
|[Open Space 2]|---|---|---|
|[Open Space 3]|---|---|---|
|[Open Space 4]|---|---|---|
|[Open Space 5]|---|---|---|
|[Open Space 6]|---|---|---|
|[Open Space 7]|---|---|---|
|[Open Space 8]|---|---|---|
