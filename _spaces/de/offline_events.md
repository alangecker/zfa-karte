---
layout: space
title:  "Analoges Programm"
svg: analog
---

Ihr seid während der Kongresstage in Leipzig und wollt nicht ausschließlich vor eurem digitalen Endgerät sitzen? Es gibt täglich mehrere nicht-digitale Veranstaltungen, zu denen wir euch herzlich einladen möchten. Nutzt die Gelegenheit, euch an der frischen Luft fortzubilden und mit anderen Zukunftsschaffenden auszutauschen.

In der folgenden Tabelle sind alle nicht-digitalen Veranstaltungen chronologisch aufgelistet. Zuvor noch die Orte in Leipzig, an denen sie stattfinden:
* Heiter bis wolkig am [**Bürgerbahnhof Plagwitz**](https://buergerbahnhof-plagwitz.de/cafe)
* Ideal: Schulze-Delitzsch-Straße 27
* Schwarze 10: Georg-Schwarz-Str. 10
* Tipi: Karl-Heine-Strasse 85-93, Einfahrt links neben Haupthalle mit Konsum, dann Eingang in rechter Ecke an Haupthalle, dort 1. Stock

|Datum|Uhrzeit|Veranstaltung|Ort|
|----|----|---------|----|
|Mi, 26.08.|15:00-16.30|<strong><a href="https://pretalx.com/zukunftfueralle/talk/MPK7UL/">Eine kurze Reise durch die Zeit</a></strong> [Workshop]|Tipi|
|Mi, 26.08.|14:30 - 16:30|<strong><a href="https://pretalx.com/zukunftfueralle/talk/EGT9JJ/">Waldweit unterwegs</a></strong> [Theater/Spaziergang]|Treffpunkt: Bushaltestelle Nonnenweg|
|Mi, 26.08.|20:30-22:00 (Einlass ab 20:00)|<a href="https://pretalx.com/zukunftfueralle/talk/BD9VZR/"><strong>RePresente – die Zukunft ist jetzt!</strong></a> [Filmabend]|Ideal|
|Mi, 26.08.|20:00-22:00|<strong><a href="https://pretalx.com/zukunftfueralle/talk/LYRJMV/">Musik unter Torbögen</a></strong> [Konzert]|Heiter bis wolkig|
|Do, 27.08.|15.00 - 16.30|<strong><a href="https://pretalx.com/zukunftfueralle/talk/8TS7W3/">Right to come! Right to go! Right to stay!</a></strong> [Workshop]|Tipi|
|Do, 27.08.|15:00-16:30|<strong><a href="https://pretalx.com/zukunftfueralle/talk/WSBWFY/">Wege zur Transformativen Kraft der Liebe</a></strong> [Konzert/Lesung]|Heiter bis wolkig|
|Do, 27.08.|15:00-18:30|<strong><a href="https://pretalx.com/zukunftfueralle/talk/FNRC9Q/">Die Utopiemaschine (1/2)</a> </strong>[interaktive Installation]|Heiter bis wolkig|
|Do, 27.08.|20:00-22:00|<strong><a href="https://pretalx.com/zukunftfueralle/talk/CZLHWZ/">Die Farben der Erde</a></strong> [Kurzfilmwanderung]|Treffpunkt: Heiter bis wolkig|
|Do, 27.08.|20:00-21:30|<strong><a href="https://pretalx.com/zukunftfueralle/talk/NW3L7C/">Collagen zum kommunistischen Begehren</a></strong> [Audiovisuelle Performance & Gespräch]|Heiter bis wolkig|
|Fr, 28.08.|11:00-12.30|<strong><a href="https://pretalx.com/zukunftfueralle/talk/97PANG/">Schönheitsideale in die Tonne treten</a></strong>[Workshop]|Tipi|
|Fr, 28.08.|15:00-18:30|<strong><a href="https://pretalx.com/zukunftfueralle/talk/A87M3Q/">Die Utopiemaschine (2/2)</a></strong> [Interaktive Installation]|Heiter bis wolkig|
|Fr, 28.08.|11:00-12:30|<strong><a href="https://pretalx.com/zukunftfueralle/talk/MAAEUT/">Die Schule von übermorgen (1/2)</a></strong> [Workshop]|Schwarze 10|
|Fr, 28.08.|15:00-16:30|<strong><a href="https://pretalx.com/zukunftfueralle/talk/PVGC9Z/">Die Schule von übermorgen (2/2)</a></strong> [Workshop]|Schwarze 10|
|Fr, 28.08.|15:00-16:00|<strong><a href="https://pretalx.com/zukunftfueralle/talk/MDGPPL/">Radioballett "good crops, bad crops" (1/2)</a></strong> [interaktive Performance + Gespräch]|Heiter bis wolkig|
|Fr, 28.08.|16:00-17:00|<strong><a href="https://pretalx.com/zukunftfueralle/talk/VRXMZC/">Radioballett "good crops, bad crops" (2/2)</a></strong> [interaktive Performance + Gespräch]|Heiter bis wolkig|
|Fr, 28.08.|17:00-17:40|<strong><a href="https://pretalx.com/zukunftfueralle/talk/ACEF3A/">Radioballett "Utopie/Dystopie</a></strong> [interaktive Performance + Gespräch]|Heiter bis wolkig|
|Fr, 28.08.|20:30-22:00 (Einlass ab 20:00)|<strong><a href="https://pretalx.com/zukunftfueralle/talk/U8LTQT/">Kurzfilmabend - meeting utopia</a></strong> [Kurzfilme &amp; Diskussion]|Ideal|
|Fr, 28.08.|20:00-21:30|<strong><a href="https://pretalx.com/zukunftfueralle/talk/ZX8HSG/">MOGA</a></strong> [Konzert]|Heiter bis wolkig|
|Sa,29.08.|11:00-12.30|<strong><a href="https://pretalx.com/zukunftfueralle/talk/JHNUTK/">Methoden der Prozessarbeit für die Gestaltung einer zukunftsfähigen Beziehungs- und Liebeskultur</a></strong> [Workshop]|tba|
|Sa, 29.08.|11.00 - 12.30|<strong><a href="https://pretalx.com/zukunftfueralle/talk/TNZYN3/">it's about empowerment - Workshop für Menschen mit Vulvina (1/2)</a></strong> [Workshop]|Schwarze 10|
|Sa, 29.08.|15:00-16.30|<strong><a href="https://pretalx.com/zukunftfueralle/talk/SQPXSW/">Anarchafeminismus</a></strong> [Workshop]|Tipi|
|Sa, 29.08.|15.00 - 16.30|<strong><a href="https://pretalx.com/zukunftfueralle/talk/JXCV3C/">it's about empowerment - Workshop für Menschen mit Vulvina (2/2)</a></strong> [Workshop]|Schwarze 10|
|Sa, 29.08.|15:00-15:45|<strong><a href="https://pretalx.com/zukunftfueralle/talk/U7KZK9/">Flashbacktour – Eine Reise durch die Zeit</a></strong> [Zeitreise a.k.a. interaktiver Spaziergang]|Heiter bis wolkig|
|Sa, 29.08.|15:45-16:30|<strong><a href="https://pretalx.com/zukunftfueralle/talk/BFUAFK/">Flashbacktour – Eine Reise durch die Zeit</a></strong> [Zeitreise a.k.a. interaktiver Spaziergang]|Heiter bis wolkig|
|Sa, 29.08.|20:00-21:30|<strong><a href="https://pretalx.com/zukunftfueralle/talk/YW9QHY/">„Die Falle“ von Rhiad Ben Amar</a></strong> [Theater für Bewegungsfreiheit]|Zukunftszentrale + [Livestream](streams/podium1)|

