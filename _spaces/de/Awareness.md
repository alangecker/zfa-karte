---
layout: space
title:  "Awareness Space"
svg: awareness
---

Herzlich Willkommen auf der Awareness-Seite des Kongresses! Hier bist du richtig, wenn du ein konkretes Anliegen hast oder, wenn du dich informieren möchtest.

#### Awareness-Konzept

Für den Kongress haben wir extra ein Awareness-Konzept geschrieben. Du findest es unter [diesem Link](https://zukunftfueralle.jetzt/awareness/) oder [hier als PDF](https://zukunftfueralle.jetzt/wp-content/uploads/2020/08/ZFA-Awarenesskonzept.pdf).

#### Awareness-Team

Wenn es dir aufgrund möglicher Grenzüberschreitungen und/oder Diskriminierungen Anderer auf dem Kongress nicht gut geht oder sonstiger emotionaler Redebedarf besteht, kannst du dich gerne an uns wenden. Wir als Awareness-Team werden dann versuchen, dir zur Seite zu stehen.

#### Erreichbarkeit des Awareness-Teams

Es gibt verschiedene Möglichkeiten, uns zu erreichen. Egal wie: Wenn du dich meldest, wird eine Person für dich gefunden werden, was kurz dauern könnte. Bitte wähle die Methode aus, die sich für dich gut anfühlt.

* Telefon: +4915735594971
* [Videokonferenz](https://kongressgelaende.zukunftfueralle.jetzt/rooms/15593140-00c7-47f0-8106-8d1b20d9d6c4){: target="_parent"}
* E-Mail: [zfa-awareness@riseup.net](mailto:zfa-awareness@riseup.net)
* Im Büro: Von 9h-21h während des Kongresses in der Klingenstr. 22 in Plagwitz
* Zugang zu BIPoC-Raum und FLINT\*-Raum bekommst du auf Anfrage über die oben genannten Wege

Aus Kapazitätsgründen kann nicht bei jeder Veranstaltung eine Person aus dem Awareness-Team direkt anwesend sein. Grundsätzlich kann die\*der verantwortliche Tech-Host angesprochen werden und dieser gibt den Kontakt der Awareness weiter.
