---
layout: space
title:  "Bezugsgruppen"
svg: groups
---
Die Bezugsgruppentreffen sind ein Angebot an Teilnehmer*innen, digital zufällige Treffen und somit vielfältigen Austausch mit anderen Teilnehmer*innen zu ermöglichen. Wenn ihr euch bei der Anmeldung für Bezugsgruppen eingetragen habt, dann wurdet ihr von uns per Los in eine Gruppe von bis zu sieben Menschen eingeteilt. Jeder Gruppe wird dann wiederum ein Gesprächsraum in unserer Kongresssoftware zugeteilt. Der Zugang ist der gleiche wie beim Workshopbereich. Das Angebot ist also Menschen vorbehalten, die sich für den Kongress angemeldet haben.

Täglich von 14:00-14:30 sind die Bezugsgruppenräume geöffnet. So könnt ihr über die Tage euren Austausch fortspinnen. Es wird auch immer wieder kleine Gesprächsanregungen aus der Vorbereitungsgruppe geben, auf die ihr euch beziehen könnt. Aber am Ende entscheidet ihr natürlich selbst, worum es in euren Treffen geht. Falls ihr in tiefe Diskussionen gleitet und mehr Zeit braucht, tragt eure Gespräche doch gerne in den [Open Space](open_spaces) und setzt sie dort fort.
