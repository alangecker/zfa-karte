---
layout: space
title:  "Kongressticker"
svg: ticker
---

<!-- <p class="rss-subscribe"><a href="{{ "/feed.xml" | prepend: site.baseurl }}">Via RSS</a> abbonieren</p> -->

Hier bleibt ihr während der Kongresstage auf dem Laufenden. Wir posten an dieser Stelle je nach Bedarf technische Updates und weisen täglich auf besondere Programmschmankerl hin. Schaut also gerne jeden Tag mal vorbei!

[**Anmeldung ist noch offen**](https://contacts.knoe.org/content/anmeldung-zukunft-fuer-alle)

[**Noch freie Workshopplätze**](https://anmeldung.zukunftfueralle.jetzt/)

---


*25.08., 18:00* **++ Zukunft Für Alle – Die Broschüre ++**

Es ist geschafft! Die Streams stehen, die Übersetzungen laufen an und funktionieren inzwischen durchgehend. Danke an alle, die das ermöglicht haben und ununterbrochen weiterhin ermöglichen: die vielen Helfer\*innen, von der Küche bis zur Regie ebenso wie die grandiosen Sprecher\*innen. Weil die Broschüre **"Zukunft Für Alle"** während des Auftaktes immer wieder genannt wurde stellen wir hier nochmal den Downloadlink rein, wünschen euch eine inspirierende Lektüre und zudem natürlich einen schönen Abend!

Achja: die Broschüre ist kostenfrei erhältich, aber nicht kostenlos entstanden. Unterstützt doch gerne das [**Crowdfunding-Projekt**](https://www.oekom-crowd.de/projekte/zukunft-fuer-alle/) zur Broschüre!

[Zukunft Für Alle Broschüre](https://zukunftfueralle.jetzt/wp-admin/upload.php?item=2888){:.btn.centered}


---

*25.08., 15:30* **++ Der Kongress geht los! ++**

Let's go! Für alle, die gleich den Kongressauftakt mit Bini Adamczak, Nina Treu und Kai Kuhnhenn streamen möchten: schaut in unseren [**Livestreamsaal Futur I**](streams/stream1). Wenn ihr die Anmeldung für Workshops verpasst haben solltet: kein Problem! Bisher sind gut 2/3 aller Plätze in Anspruch genommen. Ihr könnt euch weiterhin [**anmelden**](https://contacts.knoe.org/content/anmeldung-zukunft-fuer-alle), jedoch noch nicht für konkrete Workshops. Ab heute Abend könnt ihr euch dann [**hier**](https://anmeldung.zukunftfueralle.jetzt/) eure Workshops auswählen. Wir freuen uns auf einen inspirierenden Kongress und sechs Tage, an denen wir uns mit voller Aufmerksamkeit den Fragen widmen: Wie wollen wir 2048 leben? Und wie kommen wir dahin?

Auf eine gute Zeit!

Achja: heute Abend schon was vor? In eurem [**Lieblingslivestream**](streams/stream1) wird das Podium "Aus Krisen von heute die Zukunft entwickeln“ mit Antje von Broock, Tonny Nowshin und Tina Röthig, moderiert von Ruth Krohn übertragen.

---

*24.08., abends* **++ Zukunftszentrale und Kongressinfrastruktur werden eingerichtet ++**<br>

Ein ereignisreicher Tag neigte sich dem Ende zu, als wir gerade die Türen der Zukunftszentrale hinter uns schlossen. Der Vorbereitungskreis sah sich erstmals seit Februar in größerer Runde wieder. Doch Zeit für Pläuschchen war nur wenig. Es ist viel passiert, doch wir fassen es kurz: wir alle freuen uns riesig auf morgen und sind wahnsinnig gespannt auf den Zukunft Für Alle Kongress! Noch einmal schlafen, dann geht's los: 10:00 Pressekonferenz, 16:00 Auftaktplenum, 16:30 spricht Bini Adamczak zu "Wie Utopie?" und um 20:00 geht dann das Eröffnungspodium: "Aus Krisen von heute die Zukunft entwickeln". [Alle Veranstaltungen werden im Kanal Futur I gestreamt.](streams/stream1)

---

*20.08, 10:30.* **++ Die Karte des digitalen Kongressgeländes ist eingebettet! ++**<br>

Freund\*innen und Kämpfer\*innen der gerechten und ökologischen Zukunft, ein großer Schritt Richtung digitalem Kongress ist getan worden. Die Karte ist seit sieben Stunden online. Eingeschlafen – keine Karte. Aufgewacht – Karte! Jetzt müssen nur noch die Links darauf aktiviert, die Unterseiten fertig befüllt und alles öffentlich zugänglich gemacht werden. Das schaffen wir auch noch, keine Sorge.

Bis baldrian!

---
