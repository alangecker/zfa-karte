---
layout: space
title:  "Utopien verbinden"
svg: utopias
---
Die entstehenden Utopien auf dem Kongress leben von der Verbindung möglichst vieler Themen und Stränge sowie von der Vielfalt der Teilnehmenden. Von Mittwoch bis Freitag gibt es zwischen 17:00 bis 18:30 die Möglichkeit zum moderierten Austausch. 

Anmeldung dazu verpasst? Dann werde doch Teil von unserem „Kongressbeobachtungsteam“. Dieses        besucht möglichst viele Veranstaltungen des Kongresses und schnuppert hier und dort Atmosphäre. Sofern spontan noch Platz in "Utopien verbinden" ist, fänden wir es auch super, wenn Menschen, die Lust auf Kongressbeobachtung haben, auch "Utopien verbinden" besuchen. Die Kongressbeobachtung teilt ihre Eindrücke am Sonntag auf der Abschlussveranstaltung. Hier wird auch die gemeinsame Arbeit aus "Utopien verbinden" vorgestellt. 

Habt ihr Interesse an der Kongressbeobachtung, dann schreibt eine [**Mail**](mailto:zfa-programm@riseup.net).