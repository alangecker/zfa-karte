---
layout: space
title:  "Willkommen"
svg: welcome
---
## Willkommen beim Zukunft Für Alle Kongress!

Wie leben wir im Jahr 2048? Und wie kommen wir dahin? Auf unserem Kongress werden wir diese Fragen in Workshops und Podiumsdiskussionen, auf diversen Kulturveranstaltungen und am Tresen sowie in verschiedenen offenen Austauschräumen erörtern. Wir werden Ideen entwickeln und ausbrüten. Wir werden Wege des gesellschaftlichen Wandels entwerfen, diskutieren und anlegen. Wir werden uns über diverse gesellschaftliche Bereiche hinweg austauschen und unsere utopischen Vorstellungen verknüpfen.

Für die Teilnahme an
* [**Workshops**](workshops),
* [**Open Space**](open-spaces) und
* [**Vernetzungsräumen**](synergieraum)

ist eine Anmeldung nötig. Diese war im Vorfeld bis 20.8. geöffnet und ist ab Dienstag, 25.8. auch spontan über anmeldung.zukunftfueralle.jetzt möglich.Übersichten zu öffentlich zugänglichen Veranstaltungen findet ihr hier:

* [**Öffentliche Livestreams**](livestreams)
* [**Öffentliche nicht-digitale Veranstaltungen**](offline-events)

#### Infopunkt
Wenn ihr angemeldet seid, könnt ihr euch mit allgemeinen Fragen per Chat an den digitalen Infopunkt wenden. Er ist in der Konferenzplattform Venueless unter "Infopoint", aufgelistet am linken Rand, erreichbar. Du musst dafür auf das kleine Kompasssymbol klicken und dann dem Raum "beitreten".

Wenn du (noch) nicht angemeldet bist, wende dich mit allgemeinen Fragen, die nicht im Kongress-ABC beantwortet werden, bitte an [**zfa-checkin@knoe.org**](mailto:zfa-checkin@knoe.org).

## Hintergrund

Ein großer, offener Vorbereitungskreis hat den Kongress organisiert. Gefördert wird Zukunft Für Alle von institutionellen Förder\*innen sowie von privaten Spender\*innen. Viele weitere Infos über den Kongress findest du [**im Kongress-ABC**](https://zukunftfueralle.jetzt/kongress/kongress-abc/) am Ende dieser Seite.

Im Rahmen des Zukunft Für Alle Projektes des [**Konzeptwerk Neue Ökonomie**](https://konzeptwerk-neue-oekonomie.org) entstand eine umfassende Broschüre zur Thematik. Die findet ihr [**hier**](https://zukunftfueralle.jetzt/buch-zum-kongress/). Wir empfehlen diese von Herzen zur Lektüre – egal, ob ihr am Kongress teilnehmt oder nicht. Wir legen euch zudem den [**Kompass zur Utopieentwicklung**](https://zukunftfueralle.jetzt/wp-content/uploads/2020/08/Zukunft_fuer_alle_Kompass.pdf) ans Herz.

Nun wünschen wir euch viel Inspiration und fruchtbaren Austausch auf unserem Kongressgelände! Zur Orientierung geben wir euch nochdas Kongress-ABC an die Hand.

[Zum Workshopbereich](https://kongressgelaende.zukunftfueralle.jetzt/){:.btn target="_parent"}

