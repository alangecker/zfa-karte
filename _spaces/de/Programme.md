---
layout: space
title:  "Programm"
svg: programme
---
Auf dem Zukunft Für Alle Kongress erwarten euch mehr als 200 Workshops, 16 Fachpodien sowie vier Abendpodien zu den großen Fragen zu einer Zukunft Für Alle. Außerdem gibt es ein vielfältiges nicht-digitales Kunst- und Kulturprogramm in Leipzig. Zur Gesamtübersicht werdet ihr weiter unten weitergeleitet.

Die Workshops sind jenen Menschen vorbehalten, die für den Kongress angemeldet sind. [Wenn ihr euren per E-Mail versendeten Anmeldelink noch nicht geklickt habt: erledigt das bitte bevor ihr auf den folgenden Button klickt oder bevor ihr oben auf dieser Seite auf "Eingang" klickt.]

[Hier geht es direkt zum Workshopbereich](https://kongressgelaende.zukunftfueralle.jetzt/){:.btn.centered target="_parent"}

Doch auch für Nicht-Angemeldete gibt es vieles zu entdecken, insbesondere die Liveübertragung aller Podiumsdiskussionen. Eine Übersicht unserer öffentlichen Livestreams findet ihr [**hier**](livestreams).

Neben den verschiedenen Programmformaten ermöglicht der digitale Kongress neue Formate des Austausches. An verschiedenen Orten wird es Livestreaming-Veranstaltungen zum Kongress geben. Einen Überblick dazu findet ihr [**hier**](livestreaming-events). Zudem sind im Programm verschiedene digitale Austauschphasen eingebettet. Mehr dazu bei den [**Bezugsgruppen**](bezugsgruppen), beim [**Open Space**](open-spaces) und natürlich in der 24-Stunden-Online-Bar [**"Die Zukunft"**](bar).


[Programm im Browser](https://pretalx.com/zukunftfueralle/schedule/){:.btn.centered}

[Programm-PDF](https://www.zukunftfueralle.jetzt/wp-content/uploads/2020/08/zfa-programmuebersicht-1.pdf){:.btn.centered}
