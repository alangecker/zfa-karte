---
layout: space
title:  "Art for Utopia"
svg: art
---
Eine Utopie – wie sieht die eigentlich aus? Wie ist diese vorstellbar, erlebbar oder erprobbar? Das Kunst und Kulturprogramm des Kongresses, "Art for Utopia" stellt sich diesen Fragen und schafft Räume zum Kreieren, Einfühlen und Ermächtigen. Auf künstlerische Weise werden wünschenswerte Zukünfte gestaltet, die gerecht, ökologisch und machbar sein sollen. Wir begreifen Kunst als Element der Transformation und nicht als reines Unterhaltungsformat.

Art for Utopia erwartet euch in verschiedenen Formen: digital, offline oder beides. Die Teilnehmenden erwarten Workshops, Exkursionen, Filmabende, Konzerte, Theater, Performances und Installationen. Insbesondere, wenn ihr während des Kongresszeitraumes in Leipzig seid, laden wir euch herzlichst ein, bei nicht digitalen Veranstaltungen Kunst und Kultur zu genießen und darüber in Austausch zu kommen.

Videos und Hörspiele für verschiedene Programmpunkte von Art for Utopia findet ihr in der [**Mediathek**](mediathek).

[Programm-PDF](https://zukunftfueralle.jetzt/wp-content/uploads/2020/08/programm-download_klein_2.pdf){:.btn.centered}

**Kurzübersicht**

|Datum, Uhrzeit| Veranstaltung| Ort|
| :------------- | :----------: | -----------: |
|26.08., 11:00-12:30|**VALUE ACTING — developing an interactive street-performance for daily life based on your own values** [Workshop]|Online (Raum 6)|
|26.08., 11:00-12:30|**Gestaltungsmaterial Artivismus** [Workshop]|Online (Raum 29)|
|26.08., 14:30-16:30|**Waldweit unterwegs** [Theater & Spaziergang]|**live**, Treffpunkt: Bushaltestelle Nonnenweg|
|26.08., 15:00-17:00|**The Futurological Congress** ["Virtuelles Live Action Role Play"]|Online (Raum 7)|
|26.08., 15:00-16:30|**Turnton 2047 / Eine begehbare Erzählung** [Workshop]|Online (Raum 29)|
|26.08., 15:00-16:30|**Die Humusrevolution - ein Arbeitsstand** [Workshop]|Online (Raum 28)|
|26.08., 20:30-22:00<br>(Einlass ab 20:00)|**Filmabend: RePresente & Diskussion**|**live** im Ideal, Schulze-Delitzsch-Straße 27|
|26.08., 20:00-22:00|**Musik unter Torbögen** [Konzert]|**live** im [Heiter bis wolkig](https://buergerbahnhof-plagwitz.de/cafe)|
|26.08., 20:00-21:30|**2048 – Szenen aus einer Welt von morgen** [Hörspiel und Workshop]|Online (Raum 3)|
|27.08., 11:00-14:00|**Wie kann man den Kapitalismus überwinden, ohne ihn zu verstehen? – Utopien für Anfänger\*innen**|Online (Raum 29)|
|27.08., 15:00-16:30|**Wege zur Transformativen Kraft der Liebe** [Lesung und Musik]|**live** im [Heiter bis wolkig](https://buergerbahnhof-plagwitz.de/cafe)|
|27.08., 15:00-18:30|**Die Utopiemaschine** [interaktive Installation]|**live** im [Heiter bis wolkig](https://buergerbahnhof-plagwitz.de/cafe)|
|27.08., 15:00-16:30|**Die Tanzfläche als Carrier Bag für transformative Zukünfte** [Workshop]|Online (Raum 24)|
|27.08., 15:00-16:30|**Power Poetry, Schreibwerkstatt** [Workshop]|Online (Raum 25)|
|27.08., 20:00-22:00|**Die Farben der Erde** [Kurzfilmwanderung]|**live:** Treffpunkt: Konzeptwerk neue Ökonomie (Klingenstraße 22), bei Regen wird die Veranstaltung in den Kunstraum IDEAL verlegt. Anmeldung vor Ort , max. 40P, Die Utopiemaschine bei Regen 30P, bitte Maske mitbringen|
|28.08., 15:00-18:30|**Die Utopiemaschine** [interaktive Installation]|**live** im [Heiter bis wolkig](https://buergerbahnhof-plagwitz.de/cafe)|
|28.08., 15:00-16:00|**good crops, bad crops** [Radioballett]|**live** im [Heiter bis wolkig](https://buergerbahnhof-plagwitz.de/cafe)|
|28.08., 16:00-17:00|**good crops, bad crops** [Radioballett]|**live** im [Heiter bis wolkig](https://buergerbahnhof-plagwitz.de/cafe)|
|28.08., 17:00-17:40|**„Utopie/Dystopie“** [interaktive Performance]|**live** im [Heiter bis wolkig](https://buergerbahnhof-plagwitz.de/cafe)|
|28.08., 15:00-18:00|**Unleashing Fantasy for Transformation: Ursula K. Le Guin und die Kunst der Utopie**|Online (Raum 31)|
|28.08., 20:30-22:00<br>(Einlass ab 20:00|**Kurzfilmabend: Meeting utopia**|**live** im Ideal, Schulze-Delitzsch-Straße 27|
|29.08., 15:00-15:45|**Flashbacktour I** [Interaktiver Spaziergang]|**live** im [Heiter bis wolkig](https://buergerbahnhof-plagwitz.de/cafe)|
|29.08., 15:45-16:30|**Flashbacktour II** [Interaktiver Spaziergang]|**live** im [Heiter bis wolkig](https://buergerbahnhof-plagwitz.de/cafe)|
|29.08., 15:00-16:30|**Art for Utopia – Kollektives Arbeiten als Gesellschaftsutopie**|Online Podium im [Livestream](streams/podium1)|
|29.08., 15:00-16:30|**Art for Utopia – Kunst als Aktionsraum**|Online Podium im [Livestream](streams/podium1)|
|29.08., 20:00-21:30|**Die Falle** [Theater für Bewegungsfreiheit von Rhiad Ben Ammar]|[Zukunftszentrale](https://zukunftfueralle.jetzt/kongress/kongress-abc/#1597183083577-5cda19cb-cc99) und im [Livestream](https://zukunftfueralle.jetzt/kongress/kongress-abc/#1597183083577-5cda19cb-cc99)|
