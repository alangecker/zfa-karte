---
layout: space
title:  "Mediathek"
svg: media
---
Die große Vielfalt analoger Büchertische können wir hier leider nicht abbilden. Doch ganz ohne ergänzendes Material möchten wir nicht auskommen. Auf dieser Seite stellen wir euch Musik, Videos und Lektüre zur Verfügung, die euch durch den Kongress begleiten können. Insbesondere für Formate des Kunst- und Kulturstanges findet ihr hier einige wichtige Dateien.

Workshopmaterialien für Kongressteilnehmer*innen [**sind hier abrufbar**](https://desk.knoe.org/index.php/s/98nkY7QeS57oYwo).

Im Folgenden findet ihr zuerst die verschiedenen Medien zum [**Kunst- und Kulturstrang**](art_for_utopia) mit allen dazugehörigen Informationen. Darauf folgen einige Links und Dokumente zum Weiterlesen aus dem Projekt **["Zukunft Für Alle"](https://konzeptwerk-neue-oekonomie.org/themen/zukunftfueralle/)**. Am Ende der Seite sind diverse Dokumente aus der Kongressvorbereitung zum Download bereitgestellt

### Art for Utopia

[Art for Utopia Programme](https://zukunftfueralle.jetzt/wp-content/uploads/2020/08/programm-download_klein_2.pdf){:.btn.centered}

#### Power Poetry, Schreibwerkstatt

**Friederike Preuschen**

<iframe class="board16-9" src="https://www.youtube.com/embed/Aiag7jxu_ws" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Das Video ist Teil des gleichnamigen Workshops kann aber auch unabhängig angeschaut und umgesetzt werden.

Die Schreibwerkstatt eröffnet einen Raum empowernde, reflexive und widerständige Qualitäten des Schreibens zu erfahren. Sucht euch dafür einen Ort, an dem ihr gerne schreiben mögt. Ihr werdet mit diesem Video angeleitet experimentelle Poesie zu verfassen. Dabei geht es um einen intuitiven, spielerischen Zugang zur Sprache jenseits des rein Kognitiven und ohne Konventionen. Zufällige Assoziationen und Humor sind willkommen. Eure utopischen Zukunftsvisionen oder gesellschaftspolitische Themen, die euch aktuell beschäftigen, kommen hier auf eure persönliche Weise zum Ausdruck. Wenn ihr möchtet, könnt ihr euer Power Poem später analog oder digital präsentieren.

#### Audiotour: Imponderabilien

**Kathrin Dröppelmann, Manuel Schwiers und Luka Lenzin**

<a href="/assets/media/IMPONDERABILIEN.mp3" target="_blank">**Link zum Hörspiel**</a>

Fortbewegungsart sowie Startpunkt wählst du autonom, du brauchst lediglich Kopfhörer und ein Abspielgerät mit Zugang zur Mediathek.

Alles ist schon da. Ich treffe mit allem, was ich immer bei mir habe – meine Ohren, mein Gespür, meine Erinnerung, meine Ideen, meine Energie, meine Zeit –, auf einen Ort. Auf der 70-minütigen Audiotour animieren Lots\*innen aus verschiedenen Perspektiven zum aktiven durchlässigen Wahrnehmen und Kombinieren des (nur) scheinbar Unverknüpften. Philosophische Inputs und Wahrnehmungs-Übungen zum Thema Erinnerung nehmen darin Mal spielerisch, Mal poetisch aufeinander Bezug. Wenn eine Erinnerung an einem Ort lebt, habe ich dann immer, wenn ich dorthin zurückkomme, daran teil? Und wenn meine Erinnerung an eine Zeit geknüpft ist, trage ich dann die Zeit mit mir herum? Kontraste zwischen individuellem sinnlichem Erleben und Audiospur sind willkommen. Das Erleben wird sich für jede\*n unterscheiden – es lässt sich nicht wiederholen aber mehrmals ausprobieren, es lässt sich darüber nicht streiten aber kommunizieren.

#### Apokalypse Yesterday - Die Serie - in 4 Episoden!

**Eine Produktion des NeXt Generation Ensembles.**
**[www.theater-x.de/nextgeneration](http://www.theater-x.de/nextgeneration)**

Die Zukunft… wie geht’s der eigentlich? Ist sie müde davon, dass alle über sie reden und dass alle viel von ihr erwarten? “Die Zukunft wird‘s schon richten!”, "Scheiß auf Utopien!” sagt NeXt Generation, denn was macht die Zukunft eigentlich? Sie sitzt auf den Schultern derjenigen, die heute für eine gerechte Gegenwart kämpfen. Sie geht neben denjenigen, die Geschichten der Apokalypse bereits kennen, weil sie sie schon erleben. Das ist die Welt, in der wir leben. Vor und in der Pandemie. Heute und schon längst. Was ist mit Morgen?

*Skript/Konzept/Performance*: Next Generation Ensemble mit Cevahir Kan, David Thiery, Joshiv Puni, Luisa Schönpflug, Nele Becker, Sam., Serina Secici, Tyrese Brodöhl, Ismail Metin & Rami El

*Regie*: Annika Füser

*Dramaturgie*: Gwen Lesmeister

*Kamera*: Felipe Frozza

*Postproduktion*: Gwen Lesmeister & Annika Füser

*Musik & Sound*: Nils Erhard

*Kostümcrew*: Lua Argüello, Gugui Pellegrino, Pi Pintos, Saida Saad, Jesu Gor & Selina Thylmann

##### Folge 1: Die Zukunft in der Shishabar.
Feuer, Wasser, Feinstaub! Die Apokalypse steht vor der Tür und das obwohl schon lange davor gewarnt wird. Doch die Menschen, die die Macht haben etwas zu verändern machen einfach weiter wie gehabt.. sie können es sich halt leisten. Aber was sagt eigentlich die Zukunft dazu?

<iframe class="board16-9" src="https://www.youtube.com/embed/jHt9dms1ztA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


##### FOLGE 2: Die Wunder der Technik
"Mit neuen Technologien können wir die Welt von ihren Problemen befreien!" Doch wer produziert diese Technik und unter welchen Bedingungen? Überwachung, Kriegsmaschinerie und Geldgier... wem nutzt Technologie und für was?
Was sagt eigentlich die Zukunft dazu?

<iframe class="board16-9" src="https://www.youtube.com/embed/Grsz0-UpaFU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


##### FOLGE 3: Wunschkonzert
Wenn wir alle unsere Einzigartigkeit und individuell sind, können wir unsere Träume schon erfüllen. Doch was passiert außerhalb der Glitzerblase?

<iframe class="board16-9" src="https://www.youtube.com/embed/RogKa8ZJqo8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


##### Folge 4: Seite an Seite
So sieht die Zukunft wirklich aus: Sie geht an der Seite derjenigen, die schon jetzt für die Gegenwart kämpfen. Sie sitzt auf den Schultern derjenigen, deren reine Existenz Widerstand bedeutet.

<iframe class="board16-9" src="https://www.youtube.com/embed/ikF3LfE1xdQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

**Zum Theater X**
Mit dem Theater X existiert in Berlin-Moabit ein junges Community Theater, das von Jugendlichen und Mitarbeiter_innen gemeinsam geleitet wird. Im Zentrum des Bühnenprogramms und der Eigenproduktionen steht eine kritische künstlerische Auseinandersetzung mit gesellschaftlichen Verhältnissen aus der Perspektive marginalisierter Jugendlicher.
**[Website TheaterX](https://theater-x.com)**
**[Über das TheaterX](https://theater-x.com/uberuns)**

#### Hörspiel: Die Humusrevolution - ein Arbeitsstand

**Frieder Zimmermann**

<a href="/assets/media/humusrevolution_as_MASTER.mp3" target="_blank">**Link zum Hörspiel**</a>

Das Audiofeature kann mit dem gleichnamigen Workshop am Mittwoch kombiniert werden oder du lauschst ihm unabhängig.

Im Jahr 2050, nach mehreren Finanzkrisen, katastrophalen Dürren und Hungersnöten in Europa entscheiden sich die Menschen, nicht nur den politischen Status Quo, sondern auch das Wirtschaftssystem in Frage zu stellen. Zentraler Punkt ist dabei die Landwirtschaft und ihr Verhältnis zum Boden. Das Audiofeature berichtet aus einer möglichen Zukunft. Interviews aus der ökologischen Gemeinschaft Schloss Tempelhof u. Klein Jasedow im Lasaaner Winkel zeichnen ein hoffnungsvolles Bild einer nicht so fernen Zukunft, die real werden kann, wenn jetzt die richtigen Entscheidungen getroffen werden.

#### Hörspiel: 2048 - Szenen aus einer Welt von morgen
**Margareta Steinrücke, Elisabeth Voß, Thomas Pfaff, Walter Groeh, Gerhard Klas, Ulrich Biermann**

Eine online-Veranstaltung zum Hörspielhören und Diskutieren findet am Mittwoch Abend um 20:00 Uhr statt.

Wie sieht es in der Welt im Jahr 2048 aus - wenn es gut gelaufen ist? Ich-Erzähler*innen berichten aus ihrem Leben: Einer anderen, solidarischen, Mensch und Natur pfleglich behandelnden Welt. Nach kurzer Einführung hören wir zusammen einen 45-Minuten-Podcast aus der Zukunft. Danach möchte das Team der Audio-Utopistas und der Attac-Mini-AG Utopie mit Euch diskutieren: Was brauchen wir, um aus der alternativen Blase herauszukommen, vielen Menschen Lust auf eine andere Zukunft zu machen und Ängste zu nehmen?

### Lektüre Kongress
- **[Link Broschüre](https://zukunftfueralle.jetzt/buch-zum-kongress/)**
- **[Kompass für die Visionsentwicklung](https://zukunftfueralle.jetzt/wp-content/uploads/2020/08/Zukunft_fuer_alle_Kompass.pdf)** PDF
- **[Blog](https://zukunftfueralle.jetzt/blog/)** (einfach mal durchscrollen, es finden sich Berichte aus den meisten "Zukunftswerkstätten", die im Vorlauf zum Kongress veranstaltet wurden)
- **[Präsentation zum Kongressauftakt](https://zukunftfueralle.jetzt/wp-content/uploads/2020/08/ZFA_Hintergrund_Präsi.pdf)

### Dokumente Kongress
- **[Kongress-ABC](https://zukunftfueralle.jetzt/kongress/kongress-abc/)**
- **[Aufruf Beiträge](https://zukunftfueralle.jetzt/wp-content/uploads/2020/04/CALL_Zukunft-fuer-alle.pdf)** PDF
- **[Themenstrangbeschreibungen](https://zukunftfueralle.jetzt/wp-content/uploads/2020/04/Strang_Informationen.pdf)** PDF
- **[Leitlinien für Einreichung](https://zukunftfueralle.jetzt/wp-content/uploads/2020/04/Leitfaden_Einreichung_Workshopgestaltung-1.pdf)** PDF

### How-Tos
- **[Big Blue Button](https://zukunftfueralle.jetzt/wp-content/uploads/2020/08/Big-Blue-Button-Tipps-angenehme-Videokonferenzen.pdf)** PDF
- **[Leitfaden Teilnehmer*innen](https://zukunftfueralle.jetzt/wp-content/uploads/2020/08/LeitfadenTN.pdf)** PDF
- **[Leitfaden Tech-Hosts](https://zukunftfueralle.jetzt/wp-content/uploads/2020/08/Techhost-Anleitung.pdf)** PDF
