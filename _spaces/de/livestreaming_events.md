---
layout: space
title:  "Dezentrale Livestreamings"
svg: events
---
Wir lassen uns das enorme politische Potenzial gemeinsam erlebter Bildungsveranstaltungen nicht von Corona nehmen! Wir möchten deshalb trotz des weitestgehend digitalen Programms dazu ermutigen: kommt auch in analogen Räumen zusammen, diskutiert eure Ideen und entwickelt sie gemeinsam weiter, vernetzt euch mit anderen Interessierten und Aktiven und wendet das Gelernte in der Praxis an.

An verschiedenen Orten finden Streamingveranstaltungen unter entsprechenden Coronaauflagen statt. Seht unten nach, wo das nächstgelegene Streaming für euch ist.

#### Augsburg (Nähe)
***Adresse:*** Hauptstraße 41, 86464 Emmersacker

***Datum:*** 25.-30.8. Wir bieten an **alle Veranstaltungen** live zu streamen. Zwischen den Veranstaltungen oder auch anstatt einzelner Veranstaltungen laden wir euch ein gemeinsam in der Permakultur aktiv zu sein oder bei Spaziergängen über die Hügel und Felder über die Konferenzthemen zu sprechen.

***Programm:*** Alle [**Livestreams**](livestreams)

***Anmeldung:*** Bei Interesse bitte der [**Telegram-Gruppe beitreten**](https://t.me/joinchat/RdkH9R18sQ-OM9EF8J2_Gg), [**Kontakt**](mailto:k.jung@globalmatch.me)

***Barrierefreiheit:*** Der Streamingort befindet sich abwechselnd in den Feldern und einem Werkraum, der Werkraum ist ohne Treppenstufen zu erreichen. Es gibt eine Toilette. Bitte eine E-Mail an Katharina zu genaueren Fragen bezüglich der Barrierefreiheit.

#### Berlin
***Adresse:*** 12099 Berlin, Oberlandstr. 26-35, BUFA-Gelände

***Datum:*** 25.08., 19:00-22:30

***Programm:*** [**Eröffnungspodium**](https://pretalx.com/zukunftfueralle/talk/8W9ECS/)

***Anmeldung:*** Anmeldung beim BUFA nötig unter [**diesem Link**](https://www.eventbrite.de/e/open-air-live-streaming-eroffnungspodium-zukunft-fur-alle-kongress-tickets-116902762309), Kontakt bei [**tim@bufa.de**](mailto:tim@bufa.de)

***Barrierefreiheit:*** Zugang leider nicht barrierefrei

#### Bielefeld
***Adresse:*** 33602 Bielefeld, Kulturhaus Bielefeld e.v., Werner-Bock-Str. 34c

***Datum:*** 28.08., 20 Uhr

***Programm:*** Abendpodium: “[**Und wie kommen wir zur Utopie? – Wege der sozial-ökologischen Transformation**](https://pretalx.com/zukunftfueralle/talk/DACVVR/)

***Anmeldung:*** [**Per Mail**](mailto:carla.schmitz@t-online.de) 

***Barrierefreiheit:*** Zugang zur Veranstaltung ist barrierefrei

#### Braunschweig

Veranstaltet von: KuK-BS e.V., schrill e.V. und das Sonnensystem - Weitere Infos folgen

***Adresse:*** 

***Datum:***

***Programm:***

***Anmeldung:***

***Barrierefreiheit:***

#### Bremen
***Adresse:*** 28199 Bremen, Westerstr. 58, KlimaWerkStadt

***Datum:*** Donnerstag, 27.08. + Freitag 28., 19:45-22:15

***Programm:*** [**Abendpodium Donnerstag**](https://pretalx.com/zukunftfueralle/talk/37KQD8/) + [**Abendpodium Freitag**](https://pretalx.com/zukunftfueralle/talk/DACVVR/)

***Anmeldung:*** [**Anmeldung bitte hier**](mailto:anmeldung@klimawerkstadt-bremen.de)

***Barrierefreiheit:*** Zugang zur Veranstaltung ist barrierefrei (Toiletten leider nicht)

#### Freiburg
***Adresse:*** 79100 Freiburg, Haus des Engagements, Rehlingstraße 9

***Datum:*** 26.08., 20:00-22:00

***Programm:*** [**Abendpodium: Ökonomie der Zukunft**](https://pretalx.com/zukunftfueralle/talk/N8YVHP/)

***Anmeldung:*** [**Stadtwandler**](mailto:info@stadtwandler.org)

***Barrierefreiheit:*** Leider nein, sorry.

#### Hannover
***Adresse:*** Kötnerholzweg 13, 30451 Hannover, im Hinterhof  

***Datum:*** 25.08.-30.08., 20:00-22:00

***Programm:*** [**Alle Abendpodien sowie das Theater zum Abschluss**](https://pretalx.com/zukunftfueralle/)

***Anmeldung:*** [**Kontakt zu Kollektiv endboss**](mailto:info@endboss.eu)

***Barrierefreiheit:*** Veranstaltungsraum ja, Toilette nein

#### Leipzig \#1 (Ost)
***Adresse:*** Gartenprojekt am Bagger, Kiebitzstraße, 04349 Leipzig

***Datum:*** 25.08., 16:00 und 25.08., 20:00

***Programm:*** [**Kongressauftakt**](https://pretalx.com/zukunftfueralle/talk/WTYHHT/), [**"Aus Krisen von heute die Zukunft entwickeln”**](https://pretalx.com/zukunftfueralle/talk/8W9ECS/)

***Anmeldung:*** Anmeldung verpflichtend und telefonisch unter 01788601973

***Barrierefreiheit:*** Bedingte Barrierefreiheit: Natur/Gartengrundstück, nur teilweise mit Wegplatten, sonst unebener Erdboden/Wiesenfläche, Kompostklo nicht barrierefrei, Pinkelklo ebenerdig aber auch nur bedingt barrierefrei durch unebenen Boden

#### Leipzig \#2 (Westen)
***Adresse:*** Stadtgarten H17, Hähnelstr. 17, 04177 Leipzig-Lindenau; für den Fall technischer Probleme oder sehr windigen Wetters wäre es gut, wenn BesucherInnen vorher nochmal die aktualisierten Infos checken, ob es wie geplant stattfindet: [**Facebook-Events hier**](https://www.facebook.com/NFJLeipzig/) und [**hier**](https://www.facebook.com/Stadtgarten-H17-1551403855088744/) bzw. [**hier**](http://www.freiraumsyndikat.de/?p=termine) oder [**hier**](https://naturfreundejugend-leipzig.de/).

***Datum:*** 25.08.-30.08., 20:00-22:00

***Programm:*** [**Alle Abendpodien sowie das Theater zum Abschluss**](https://pretalx.com/zukunftfueralle/)

***Anmeldung:*** Ohne Anmeldung, aber: leider müssen auch wir die Anzahl der Gäste im Stadtgarten momentan begrenzt halten und das Tor schließen, wenn es zu voll wird. Bitte beachtet auch unser [**Hygienekonzept**](http://www.freiraumsyndikat.de/?p=blog&id=28).

***Barrierefreiheit:*** Bedingte Barrierefreiheit (Gartengrundstück, ebenerdig aber teilweise eng, Kompostklo nicht barrierefrei) 

#### München
***Adresse:*** 80336 München, Schwanthalerstraße 80, EineWeltHaus München: Dachterrasse (bei schönem Wetter)/Raum 211 (bei schlechtem Wetter)

***Datum:*** 26.08., 20:00-22:00 (Einlass 19:30)

***Programm:*** [**Abendpodium: Ökonomie der Zukunft**](https://pretalx.com/zukunftfueralle/talk/N8YVHP/)

***Anmeldung:*** [**Anmeldung bitte hier**](anmeldung@klimaherbst.de)

***Barrierefreiheit:*** Zugang barrierefrei

#### Rheinland (Lützerath)
***Adresse:*** Mahnwache Lützerath an der Landstraße 277 (in 41812 Erkelenz)

***Datum:*** 25.08. und 28.08., ab 20:00

***Programm:*** [**Eröffnungspodium**](https://pretalx.com/zukunftfueralle/talk/8W9ECS/) und [**Abendpodium**](https://pretalx.com/zukunftfueralle/talk/DACVVR/) 

***Anmeldung:*** Erstmal nicht nötig, aber Infos unter Mahnwacheninfotelefon: 0152 01339091

***Barrierefreiheit:*** Ja

#### Komplex Schwerin
***Adresse:*** Pfaffenstr. 4, 19055 Schwerin

***Datum:*** 26.08. 20 Uhr

***Programm:*** [**Abendpodium: Ökonomie der Zukunft**](https://pretalx.com/zukunftfueralle/talk/N8YVHP/)

***Anmeldung:*** [**info@bundjugend-mv.de**](mailto:info@bundjugend-mv.de)

***Barrierefreiheit:*** Leider nein.

#### Taucha, OT Sehlis
***Adresse:*** 04425 Sehlis, An der Schmiede 4

***Datum:*** 25.08., 26.06., 27.08.

***Programm:*** [**Abendpodien**](http://www.schmiede4.net/?q=ZukunftFuerAlle)

***Anmeldung:*** [**Werkstatt für nachhaltiges Leben und Arbeiten e.V.**](mailto:wfnlua@posteo.de)

***Barrierefreiheit:*** Veranstaltungsraum ja, Toilette nein

