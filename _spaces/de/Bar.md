---
layout: space
title:  Online-Bar „Die Zukunft“
svg: bar
accordion:
  - title: Einführung
    content: >
      Hattest du schon einmal eine anregende Diskussion oder ein intimes Gespräch in einem Videokonferenzraum mit hundert Teilnehmer\*innen? Nein? Wie denn auch. Um trotz der räumlichen Distanz kleine Runden zu ermöglichen, dabei aber trotzdem nicht den Anschluss an die "unbekannten Anderen" zu verlieren, wurde *die Zukunft* geschaffen. Sie ist ein Konstrukt aus mehreren (virtuellen) Räumen, durch das du dich bewegen kannst und - falls du etwas länger bleibst - sogar musst. Denn es gilt: So wie du dich im nicht-virtuellen Leben durch eine Bar oder ein Café bewegst, wirst du es auch in der *Zukunft* tun. Wenn du dich nicht gerade verabredet hast, ist der *Innenhof* ein guter Ort zum Ankommen. Du hast Durst? Bevor du zu deinem echten Kühlschrank (oder wohin auch immer) gehst, verlässt du den virtuellen Raum, in dem du gerade bist und gehst zum *Tresen*. Erfahrungsgemäß sammeln sich hier immer eine ganze Menge Leute. Du kannst hier bleiben oder dich mit deinem Getränk etwa in die *überdachte Sofaecke* setzen oder einfach mal nachsehen, was so in den *Themenräumen* vor sich geht. Und wichtig auch: Wenn du auf die Toilette musst, dann stehe auch nicht einfach vor deiner Kamera auf, sondern wechsle vorher zur *Toilette* und lass deine Kamera hier auch gerne aus.
      
      
      Wie im nicht-virtuellen Leben bist du auch in der Zukunft nie in mehreren Räumen gleichzeitig. Dadurch wissen alle anderen in den entsprechenden Räumen immer genau, ob du gerade da bist oder nicht. Dann spricht dich auch keiner an, wenn du weg bist und du gibst anderen die Möglichkeit, über Dinge zu sprechen, die dich nicht interessieren. Durch eure Bewegung im Raum können immer neue Gesprächskonstellationen entstehen und du lernst auch neue Menschen kennen. Ganz nebenbei hast du so auch immer eine Ausrede, eine Runde zu verlassen und dir neue Gesprächspartner\*innen zu suchen, wenn dir zum Beispiel das Thema gerade nicht gefällt. Du bist skeptisch, dass das Konzept auch wirklich so funktioniert? Vertrau mir: Nach ein paar Stunden fühlt sich das alles sehr natürlich an.
      
      
      Und noch etwas: Da die räumliche Distanz doch das Zwischenmenschliche etwas erschwert, ist es von Vorteil, einfach alle Anwesenden wie alte Freund\*innen zu behandeln. Das macht wirklich vieles einfacher und sehr viel netter!
  - title: Vorbereitungen und Regeln
    content: >
      Um in der Zukunft dabei zu sein, musst du dich nirgendwo anmelden. Trotzdem solltest du hier einiges beachten:

      * Benutze bitte den Browser **Chrome/Chromium**, wenn du in die Kneipe gehst. Die verwendete Software Jitsi hat leider ein Problem mit Firefox/Safari und die Verbindung kann für alle wesentlich schlechter werden, wenn du nicht Chrome/Chromium nutzt.
      
      * Beachte es **die Räume zu wechseln** wie du es auch im nicht-virtuellen Leben machen würdest. Lasse am besten diese Seite offen, klicke auf den Link des jeweiligen Raumes, schließe den Tab, wenn du ihn wieder verlässt und wechsel von hier in den nächsten Raum.
      
      * Trage unbedingt ein **Headset** oder **Kopfhörer**. Da ihr in *der Zukunft* auch gemeinsam Musik hören könnt, führt alles andere zu fürchterlichen Rückkopplungen. Bitte halte dich daran.
      
      *  Lasse dein **Mikrofon angeschaltet**. Anders als in Workshops und Konferenzen soll lachen/schmatzen/rauchen zu hören sein und trägt zur Atmosphäre bei. Außerdem ist es viel entspannter sich zu unterhalten, wenn nicht ständig das Mikro an- und ausgeschaltet werden muss.
      
      * Beachte bitte die **Regeln der einzelnen Räume**. Die Regeln sind Rahmenbedingungen, durch welche *die Zukunft* erst lebendig wird.

      Die Zukunft ist selbstorganisiert, unabhängig von venueless und hat keine Betreuer\*innen. Nur wenn sich Leute (du könntest dazu gehören!) aktiv einbringen und etwa die gemeinsamen Musikkanäle verbreiten oder Diskussionsvorschläge bringen, kann die Zukunft auch lebendig werden.

  - title: Haupträume
    content: >
      Die Zukunft wurde in einem alten Gemäuer errichtet. Du betrittst sie durch ein morsches Holztor, schreitest einen modrig riechenden Flur mit viel zu vielen abgestellten Fahrrädern entlang und betrittst schließlich den Ort, an dem alle immer wieder zusammenkommen: Den *Innenhof*. Palettenbänke, Sitzkissen, Schattenplätze für den Tag und Lampionketten für die Nacht. Ein großer Raum für alle.
      
      
      Direkt gegenüber des Eingangs siehst du einen behelfsmäßig aufgebauten *Stehtresen*, an dem viele nur schnell ihre Getränke holen und angebaut den *Sitztresen* für alle, die es leid sind für jedes Getränk immer wieder aufzustehen. Falls es regnet oder sich einfach eine kleinere Gruppe zurückziehen möchte, gibt es eine *überdachte* *Sofaecke.* Und letzten Endes, damit auch niemand die Idylle stört, ist auch der Weg zur *Toilette* nicht weit. In den *abschließbaren Kabinen* kann auch gemeinsam aufs Klo gegangen werden.

      Directly opposite the entrance you will see a makeshift standing counter where many people quickly fetch their drinks. Right next to that counter you find the seat counter for all those who are tired of getting up again and again for every drink. In case of rain or if a smaller group just wants to retreat, there is a covered sofa corner. And in the end, so that nobody dares to disturb the idyll, the way to the toilet is not far either. In the lockable cabins it is also possible to go to the toilet together.

      * [**Innenhof**](https://meet.ffmuc.net/Z-Innenhof)
      
      * [**Stehtresen**](https://meet.ffmuc.net/Z-Innenhof)
      
      * [**Sitztresen**](https://meet.ffmuc.net/Z-Sitztresen)

      * [**Überdachte Sofaecke**](https://meet.ffmuc.net/Z-Sofaecke)

      * [**Pinnwand**](https://pad.elevate.at/p/zukunft-pinnwand) --> Die Pinnwand ist auch am unteren Ende dieser Seite auffindbar.


      **Toilettenräume**
    
      * [**Toilette**](https://meet.ffmuc.net/Z-Toilette)

      * [**Kabine 1**](https://meet.ffmuc.net/Z-Kabine1) 

      * [**Kabine 2**](https://meet.ffmuc.net/Z-Kabine2)
    
      * [**Kabine 3**](https://meet.ffmuc.net/Z-Kabine3)
      
      
      *Technische Anmerkung: Du kannst einen Raum (z.B. die Kabine) abschließen, indem du unten auf das Schild (Sicherheitsoptionen) klickst und dann ein Passwort festlegst. Das Passwort verfällt, wenn der Raum einmal komplett leer ist.*
  - title: Themenräume
    content: >
      In Zweierpaaren teilen sich je zwei Themenstränge des Kongresses einen Raum, wobei mit dem Finanzsystem niemand gemeinsame Sache machen wollte. Über die Verbindung von Themensträngen des Kongresses mit virtuellen Räumen soll ermöglicht werden, dass direkt nach Vorträgen/Workshops in *der Zukunft* darüber gequatscht werden kann und dieselben Teilnehmer\*innen auch schneller wieder zusammenfinden. Abgesehen davon sind alle Räume immer und von jeder und jedem frei nutzbar, wenn auch manche Räume (der Debattierclub und die Tanzfläche insbesonders) spezielle Regeln haben und es auch wichtig wäre, diese zu beachten. Jeder der Räume verfügt dabei auch über einen Nebenbereich. Das kann zum Beispiel helfen, wenn ihr euch in einem Raum verabredet und dieser schon voll ist oder wenn ihr euch eher in privater Runde treffen wollt. Für eure Privatssphäre dürfen Nebenräume immer gerne mit einem Passwort geschützt werden.


      * **Debattierclub** (Bildung: Schule Macht Zukunft & Die ganze Arbeit)

        Der vorderste Raum des ersten Stockwerkes wurde offensichtlich einst als Unterrichtsraum genutzt und ist noch in gutem Schuss. Die Tafel ist gewischt, die Tische auf die Seite geschoben und in der Mitte ein großer Stuhlkreis aufgestellt. Der Raum eignet sich also besonders für Diskussionen aller Art. Damit diese nicht zufällig entstehen müssen, gibt es auf der [Pinnwand] einen extra Bereich, in dem du frei eintragen kannst, was du gerne (wie) diskutieren würdest. Egal ob inhaltliche Debatte oder eigene Ideen für eine bessere Welt: Beschreibe kurz deinen Punkt, schlage am besten mehrere Terminoptionen vor und warte, ob sich Interessent\*innen dafür finden. Falls ein solcher Diskussionstermin festgesetzt wurde, sollte der Raum hierfür freigehalten werden.


        [**Debattierclub**](https://meet.ffmuc.net/Z-Debattierclub)
        
        [**Debattierclub Nebentisch**](https://meet.ffmuc.net/Z-Debattierclub-2)        
        
        [**Pinnwand**](https://pad.elevate.at/p/zukunft-pinnwand)


      * **Planungsbüro mit Leinwand** (Gesellschaftsorganisation & Digitalität)

        Ebenfalls im ersten Stock befindet sich ein verstaubtes Planungsbüro, in welchem zu früheren Zeiten wohl wichtige Entscheidungen getroffen wurden. Allen Anschein nach haben seitdem mehrere Gruppen versucht dem Raum eine neue Richtung zu geben, doch da sich nie auf etwas geeinigt werden konnte, wurde daraus eine bessere Abstellkammer. In den letzten Vorbereitungstagen zum *Zukunft für Alle-Kongress* haben jedoch ein paar Leute einen Beamer abgezweigt, ein Bettlaken aufgehängt und an den Feierabenden kleine Filmabende veranstaltet. Während es oft also eher unhöflich ist, einfach anderen Leuten lustige youtube-Videos vorzusetzen, ist der Raum hier wie geschaffen, um sich gemeinsam zurückzulehnen. Wer den gemeinsamen Medienkonsum nicht dem Zufall überlassen will, kann natürlich Vorschläge an die *Pinnwand* schreiben.



        [**Planungsbüro**](https://meet.ffmuc.net/Z-Planungsb)
        
        [**Planungsbüro Nebencouch**](https://meet.ffmuc.net/Z-Planungsb-2)
        
        [**Pinboard**](https://pad.elevate.at/p/zukunft-pinnwand)
        
        *Technische Anmerkung: youtube-videos können direkt in Jitsi eingespeist werden (bei den drei Punkten). Wird ein Video abgespielt, werden die Mikrofone automatisch stumm geschaltet - einfach Leertaste gedrückt halten, um sprechen zu können.*


      * **Kubrick's Milchar** (Wohnen & Kunst und Kultur)

        Es ist kaum zu glauben, aber jemand scheint im zweiten Stock *der Zukunft* tatsächlich einen Raum eingerichtet zu haben, der auch nach Zukunft aussieht. Offensichtlich hat eine große Kubrick-Kennerin Unmengen an Requisiten aus "2001: Odysee im Weltraum" und "Clockwork Orange" nachgebaut und damit einen einzigartigen Wohnraum konzipiert. Weiße Möbel und indirektes Licht rahmen ihre Sammlung ein, die sie liebevoll "Kubricks Milchbar" getauft hat. Der große Vorteil: In der Milchbar gibt es nicht nur Milch und wer hier ist, muss nicht an den Tresen im Innenhof. Einfach nur der Getränke wegen aber hineinzuplatzen ist diesem Raum unwürdig und sollte tunlichst unterlassen werden.


        [**Kubrick's Milchbar**](https://meet.osna.social/Z-Kubricks-Milchbar)
        
        [**Kubrick's Milchbar Tresen**](https://meet.osna.social/Z-Kubricks-Milchbar-2)


      * **Kissenlounge** (Welt der Beziehungen & Soziale Garantien)

        Hier ist es warm. Hier ist es weich. Hier ist es kuschlig und besonders ist es hier äußerst gemütlich. Kein Tisch ist höher als das Knie, kein Platz existiert, bei dem nicht zig Kissen in unmittelbarer Greifweite sind. Das hier ist ein guter Ort. Ein bequemer Ort. Ein Ort, an dem auch möglich sein sollte, unbequemes auszusprechen und Gefühle offenzulegen.



        [**Kissenlounge**](https://meet.freifunk-aachen.de/Z-Kissen-Lounge)

        [**Matratzenecke**](https://meet.freifunk-aachen.de/Z-Kissen-Lounge-2)


      * **Bei den Solarpanels** (Energie und Klima & Globale Gerechtigkeit)

        Über eine kleine Luke kann das Dach des Gebäudes betreten werden und wer die richtige Uhrzeit abpasst, erlebt hier die schönsten Sonnenuntergänge. Zwischen Solarpanels und gemauertem Schornsteinen lässt sich ein Tag ausgezeichnet reflektieren.


        [**Bei den Solarpanelen**](https://meet.freifunk-aachen.de/Z-Solarpanels)
        
        [**Neben den Solarpanelen**](https://meet.freifunk-aachen.de/Z-Solarpanels-2)


      * **Der Turm** (Finanzsystem)
      
        Der Turm sieht hoch aus, doch beim Weg nach oben könnte ihm die doppelte Höhe zugeschrieben werden. Endlich auf der höchsten Ebene angekommen, ist der Turm deutlich heruntergekommener als gedacht: Jeder einzelne Fleck des Raumes wurde mit Graffiti beschmiert und alle Graffitis bestehen lediglich aus Zahlen. Zahlen, Zahlen, Zahlen überall. Zahlen über dir, Zahlen rechts von dir, Zahlen links von dir und Zahlen in deiner Nase - überall nur Zahlen. Und wenn du vom Turm hinuntersiehst, dann siehst du Zahlen, die über den Innenhof streifen. Oder sind es Menschen? Von hier oben ist das schwer zu unterscheiden.

      
      
        [**Turm**](https://meet.ffmuc.net/Z-Turm)
      
        [**Bank neben dem Turm**](https://meet.ffmuc.net/Z-Turm-2)


      * **Tanzkeller** (Bewegungsfreiheit & Mobilität von unten)
      
        Direkt aus dem Innenhof führt eine steinere Treppe nach unten in die Dunkelheit und manchmal in der Nacht sind daraus deutlich Bässe zu vernehmen. So unwahrscheinlich es klingt, so magische Momente gab es bereits, in denen zu später Stunde jemand vorgeschlagen hat, virtuell tanzen zu gehen. Und dann wurde die Musik synchronisiert, das Licht in jedem eigenen Raum gedämpft, die Mikrofone ausgeschalten und stundenlang zwar im eigenen Zimmer, aber dann doch gemeinsam getanzt. Und so seltsam das klingen mag, so schön ist es gewesen. Wer wird sich wagen, auf diesem Kongress die Tanzfläche zu eröffnen?
        
        
        [**Tanzfläche**](https://meet.ffmuc.net/Z-Tanzflaeche)
        
        [**Dunkle Ecke**](https://meet.ffmuc.net/Z-Tanzflaeche-2)
        
        
        *Wichtige Anmerkung: Falls du nachts hier bist - also ihr euch nicht nach einem Workshop zum diskutieren trefft - dann sorge bitte dafür, dass dein eigenes Zimmer beim Tanzen unbedingt **abgedunkelt** ist, um eine entsprechende Atmosphäre entstehen zu lassen. **Schalte dein Mikrofon unbedingt aus und lasse es auch aus** - Gespräche werden über den Chat geführt. Wenn jemand sein Mikrofon ständig benutzt, darf diese Person aus dem Raum hinausbefördert werden (die erste Person, die den Raum betreten hat, kann das). Und sonst: Viel Spaß hier!*


      * **Die Tafel** (Landwirtschaft und Ernährung & Solidarisch Wirtschaften)
        
        Ein kleiner Gang führt aus dem Innenhof hinaus zu einem groß angelegten Gemüsebeet, neben dem eine lange Tafel mit weißem Tischtuch aufgebaut ist. Wenn es genug für alle gibt und alle auch darüber verfügen können, dann ist es eine erstrebenswerte Aufgabe den Kürbissen beim wachsen zuzusehen. Genau das ist hier möglich. Wer nicht gerne alleine isst, kann sich hier zum Mittagessen (zwischen 12:30 und 14:00), Kaffeetrinken (16:30 - 17:00) oder Abendessen (18:30-20:00) treffen oder einfach dazustoßen um herauszufinden, was die anderen so für sich zubereitet haben.

        
        
        [**Tafel beim Gemüsebeet**](https://meet.ffmuc.net/Z-Tafel)


        [**Tafel Nebentisch**](https://meet.ffmuc.net/Z-Tafel-2)
  - title: Musik
    content: >
      Die Zukunft ist dezentral und selbstorganisiert. Das Online-Tool [[sync-tube]](https://sync-tube.de/) ermöglicht es euch gemeinsam youtube-Songs zu hören. Damit aber immer mehr Leute mit euch zusammen dieselbe Musik erleben können (etwas von höchster Bedeutung für das wirkliche Gefühl von Kneipe!) müsst ihr dafür sorgen, dass die Links zu den sync-tube-Räumen sich verbreiten. Und da niemand weiß, wer in welchem sync-tube-Raum ist bzw. wer überhaupt Musik hört, solltet ihr euch an eine einfache Regel halten: Immer wenn ihr einen Raum betretet, in dem andere anwesend sind, kopiert den Link zu eurem sync-tube-Raum in das Chat-Fenster von Jitsi. Ohne es groß ansprechen zu müssen, können andere dann reinhören und entscheiden, ob sie selbst in diesem Kanal bleiben wollen und ihn dann dementsprechend auch weiterverbreiten.

      
    Also kurz: Du willst mit anderen Musik hören?

      * Erstelle einen Raum auf [sync-tube.de](https://sync-tube.de/) oder betritt einen vorhandenen Raum über einen Link.
      
      * Suche auf youtube.com nach Liedern und kopiere sie in den sync-tube-Raum. Du kannst auf sync-tube selbst leider nicht nach Liedern suchen.
      
      * Teile deinen sync-tube-Link immer unaufgefordert, wenn du einen Raum in *der Zukunft* betrittst.
      

      Auf diese Weise können in einer gut gefüllten Bar verschiedene "Radiosender" im Umlauf sein. Zusätzlich könnt ihr eure sync-tube-Links auch auf die Pinnwand schreiben und es so mehr Teilnehmenden ermöglichen mitzuhören. Sobald ein sync-tube-Kanal einmal leer ist, ist dieser Kanal nicht mehr betretbar und ein neuer muss eröffnet werden.

  - title: Tipps und Tricks
    content: >
      * **Was sind die Öffnungszeiten *der Zukunft*?** *Die Zukunft* ist 24 Stunden am Tag geöffnet. Die höchste Wahrscheinlichkeit jemandem zu begegnen, ist allerdings am Abend.
      
      
      * **Wieso begegne ich selten jemanden?** Wenn jede\*r nur die Räume durchklickt und nirgends bleibt, wird nie jemand irgendjemanden treffen. Wenn du nicht verabredet bist, ist es daher auch wichtig, einfach mal länger an einem Ort alleine zu bleiben. Der Tresen und der Innenhof sind Anlaufstellen hierfür.
      
      * **Ich möchte z.B. im Debattierclub auf andere warten, aber mich währenddessen unterhalten. Geht das?** Ja. Technisch kannst du über zwei Tabs auch in zwei verschiedenen Räumen sein. Wenn du explizit auf etwas/jemanden wartest, kannst du das dort mit ausgeschalteten Mikro und Kamera machen, während du aktiv z.B. am Tresen sitzt und mit anderen so lange quatscht. Versuche so etwas aber möglichst kurzzuhalten und beachte die sonst geltende Regel, niemals aktiv in zwei Räumen gleichzeitig zu sein. Wenn du also z.B. auf die Toilette gehst, musst du deinen aktiven Raum unbedingt verlassen und nicht nur die Kamera ausschalten. Geb anderen die Möglichkeit sich zu unterhalten ohne dem Gefühl, jemand könnte ihnen zuhören.
    
    
       * **Jitsi ist überlastet - was tun?** Zuerst abfragen, ob jemand einen anderen Browser als Chrome/Chromium verwendet. Andere Browser verbrauchen oft die dreifache Bandbreite. Ansonsten Video-Qualität auf Low stellen oder Kamera ganz ausschalten. Besser kein Bild als ewige Diskussionen über schlechte Technik. Und letzte Möglichkeit: Wechselt gemeinsam in einen Raum, der auf einem anderen Server läuft. Folgende Räume haben alternative Server zum Freifunk München: "Kubricks Milchbar" (Chaostreff Osnabrück), "Kissen-Lounge" und "Bei den Solarpanels" (Freifunk Aachen)
     
     
      * **Kamera hängt fest - was tun?** Wenn du immer nur dieselbe Person siehst, die Kamera also nicht zwischen den Redenden wechselt, dann gehe auf das Symbol für die geteilte Ansicht (unten in der Leiste) und von dort dann wieder zurück - Problem behoben.
     
     
      * **Wie kann ich mich ungestfört mit jemanden unterhalten?** Wenn ihr ein privates Gespräch führen wollt, könnt ihr einen Raum mit einem Passwort schützen. Klickt dafür einfach auf das Schildsymbol in der unteren Leiste und legt das Passwort fest. Das Passwort verfällt, sobald der Raum einmal leer war. 
     
      * **Wie kann ich etwas zu jemanden privat sagen?** Du willst etwas mit jemanden besprechen, ohne, dass alle es mitbekommen sollen? Zum Beispiel, in welchem Raum ihr dem aktuellen Gespräch entfliehen wollt? Benutzt die Funktion der privaten Nachrichten, indem ihr auf die drei Punkte auf dem entsprechenden Bildsymbol der Teilnehmenden klickt.
     
     
       * **Ich habe mich in Raum [xy] mit jemanden verabredet, aber dort sind andere Leute - was tun?** Versucht einfach den Nebentisch des entsprechenden Raumes zu nehmen. Falls auch dort niemand ist, könnt ihr euch über die [Pinnwand] wieder finden.
     
     
       * **Ich habe ein anderes Problem, einen Verbesserungsvorschlag oder ein anderes Anliegen. Wohin kann ich mich wenden?** Sobald die Zukunft steht, ist sie ein selbstorganisierter Ort, dem auch niemand aus dem Orga-Team direkt zugeordnet ist. Schreibe dein Anliegen am besten auf die Pinnwand und sieh auch immer wieder dort nach, was andere gerade stört bzw. welche Verbesserungsvorschläge sie haben. Falls das nichts hilft, kann sich an das [Technik-Team](Tech_Support) gewendet werden.
     
     
      * **Ein kalter Winter steht bevor - gibt es auch eine Online-Kneipe für nach den Kongress?** Der *[Schwarze Krauser](https://pad.elevate.at/p/schwarzer-krauser)* ist selbstorganisiert und das ganze Jahr geöffnet. Und wer weiß, vielleicht bleibt die Zukunft ja auch erhalten?!

---

Willkommen in der Zukunft! Da wir gegenwärtig nur schwer zusammensitzen können, bleibt uns nur die Flucht in die Zukunft und ich will sagen: Es gibt schlimmeres! Für eine Woche ist die Zukunft dein social-space, um mit deinen vielleicht weltweit verteilten Leuten abzuhängen, dich zwischen den Veranstaltungen mit Personen zu treffen, die du ganz gut findest oder die Zukunft ist einfach nur der Ort, an dem du abends am Tresen stehen und dir mit anderen Kongressteilnehmer*innen die Nacht um die Ohren schlagen kannst. Denn das Beste ist: Die Zukunft ist 24 Stunden am Tag für dich geöffnet! Das große Problem: Deine Getränke musst du dir selbst mitbringen.

Bei deiner ersten Ankunft in der Zukunft empfehlen wir dir: nimm dir das Getränk deiner Wahl zur Hand und lies die folgenden Erläuterungen zum Barkonzept durch. In diesem Sinne: gute Zeit, Prost!

{% include accordion.html %}

**Hier nochmal alle Räume alphabetisch sortiert:**

* [**Pinnwand**](https://pad.elevate.at/p/zukunft-pinnwand)

* [**Bank neben dem Turm**](https://meet.ffmuc.net/Z-Turm-2)
* [**Bei den Solarpanelen**](https://meet.freifunk-aachen.de/Z-Solarpanels)
* [**Debattierclub**](https://meet.ffmuc.net/Z-Debattierclub)
* [**Debattierclub Nebentisch**](https://meet.ffmuc.net/Z-Debattierclub-2)
* [**Innenhof**](https://meet.ffmuc.net/Z-Innenhof)
* [**Dunkle Ecke**](https://meet.ffmuc.net/Z-Tanzflaeche-2)
* [**Kabine 1**](https://meet.ffmuc.net/Z-Kabine1)
* [**Kabine 2**](https://meet.ffmuc.net/Z-Kabine2)
* [**Kabine 3**](https://meet.ffmuc.net/Z-Kabine3)
* [**Kissenlounge**](https://meet.freifunk-aachen.de/Z-Kissen-Lounge)
* [**Kubrick's Milchbar**](https://meet.osna.social/Z-Kubricks-Milchbar)
* [**Kubrick's Milchbar Tresen**](https://meet.osna.social/Z-Kubricks-Milchbar-2)
* [**Matratzenecke**](https://meet.freifunk-aachen.de/Z-Kissen-Lounge-2)
* [**Neben den Solarpanelen**](https://meet.freifunk-aachen.de/Z-Solarpanels-2)
* [**Debattierclub**](https://meet.ffmuc.net/Z-Debattierclub)
* [**Planungsbüro**](https://meet.ffmuc.net/Z-Planungsb)
* [**Planungsbüro Nebencouch**](https://meet.ffmuc.net/Z-Planungsb-2)
* [**Sitztresen**](https://meet.ffmuc.net/Z-Sitztresen)
* [**Stehtresen**](https://meet.ffmuc.net/Z-Innenhof)
* [**Tafel beim Gemüsebeet**](https://meet.ffmuc.net/Z-Tafel)
* [**Tafel Nebentisch**](https://meet.ffmuc.net/Z-Tafel-2)
* [**Tanzfläche**](https://meet.ffmuc.net/Z-Tanzflaeche)
* [**Toilette**](https://meet.ffmuc.net/Z-Toilette)
* [**Turm**](https://meet.ffmuc.net/Z-Turm)    
* [**Überdachte Sofaecke**](https://meet.ffmuc.net/Z-Sofaecke)

#### Die Pinnwand
<iframe class="board3-2" name="embed_readwrite" src="https://pad.elevate.at/p/zukunft-pinnwand?showControls=true&showChat=true&showLineNumbers=false&useMonospaceFont=false"></iframe>
