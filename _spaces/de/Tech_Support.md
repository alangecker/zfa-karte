---
layout: space
title:  "Tech-Support"
svg: support
---
Hallo, herzlich willkommen!

Wir sind da, um möglicherweise auftretende technische Probleme während deines Kongressaufenthaltes schnellst möglich zu beheben. Da jede*r von uns immer nur einer Person auf einmal weiterhelfen kann, sind unsere Ressourcen begrenzt. Deshalb haben wir auf dieser Seite verschiedene Anleitungen hochgeladen, die für eine technische Verbesserung deines Kongresserlebnisses hilfreich sein könnten:

- [**Leitfaden Teilnehmer*innen**](https://zukunftfueralle.jetzt/wp-content/uploads/2020/08/LeitfadenTN.pdf)
- [**Anleitung Big Blue Button**](https://zukunftfueralle.jetzt/wp-content/uploads/2020/08/Big-Blue-Button-Tipps-angenehme-Videokonferenzen.pdf)
- [**Anleitung Tech-Hosts**](https://zukunftfueralle.jetzt/wp-content/uploads/2020/08/Techhost-Anleitung.pdf)

Wenn das nicht weiterhilft, benutze bitte einen der Techsupport-Chatkanäle auf "venueless" oder ruf uns an unter +4934165673750. Wir bitten um  Verständnis, dass wir aus Kapazitätsgründen individuelle Probleme nur für angemeldete Teilnehmer*innen bearbeiten können.