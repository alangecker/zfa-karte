---
layout: space
title:  "FLINT* space"
svg: flint
---

## Hallo! Willkommen im FLINT\*-Space des Kongresses!

Wenn du möchtest, kannst du dich mit Namen und Pronomen vorstellen. FLINT\* ist die Abkürzung für Frauen, Lesben, inter*, nichtbinär und trans
\- wenn das nicht für dich passt, bitten wir dich, zu gehen.
Wir wollen einen sicheren Raum schaffen, in dem gleichzeitig auch Wut über die Verhältnisse ihren Platz haben darf. Bei diskriminierenden Äußerungen behalten wir uns jedoch die Moderation vor.

[Zum FLINT\*-Space](flint-space)[:.btn.centered]