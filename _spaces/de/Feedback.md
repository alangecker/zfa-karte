---
layout: space
title:  "Feedback"
svg: feedback
---
Wir freuen uns über Feedback zu Atmosphäre, Inhalten und Technik. Schreibt uns gerne anonym über unser Kontaktformular. [**Das findet ihr hier**](https://zukunftfueralle.jetzt/kontakt/#feedback). Denkt bitte dran, in eurer Nachricht eine Mailadresse anzugeben, falls ihr eine Antwort haben möchtet.
